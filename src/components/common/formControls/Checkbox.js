import React from 'react'
import { observer } from 'mobx-react'
import { SelectionControl } from 'react-md'

export default observer(({ field }) => (
    <SelectionControl
        id={field.id}
        label={field.label}
        labelBefore={true}
        name={field.name}
        //this is invoking the mobx-react-form onChange event directly
        //we need this to handle our controls
        onChange={(value, e) => field.onChange(value)}
        type={field.type} />
))