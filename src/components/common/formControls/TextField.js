import React from 'react'
import { observer } from 'mobx-react'
import { TextField } from 'react-md'

export default observer(({ field }) => (
    <TextField
        {...field.bind()}
        className='md-cell'
        error={field.hasError}
        errorText={field.error}
        id={field.id}
        label={field.label}
        //this is invoking the mobx-react-form onChange event directly
        //we need this to handle our controls
        onChange={(value, e) => field.onChange(value)}
        placeholder={field.placeholder}
        name={field.name} />
))