import React from 'react'
import { CircularProgress } from 'react-md'
import PropTypes from 'prop-types'

const LoadingProgressIndicator = (props) => (
    <div className='loading-progress-container'>
        <CircularProgress
            id={props.id}
            scale={props.scale}/>
        <h3>{props.loadingMessage}</h3>
    </div>
)

LoadingProgressIndicator.propTypes = {
    id: PropTypes.string,
    scale: PropTypes.number
}

export default LoadingProgressIndicator