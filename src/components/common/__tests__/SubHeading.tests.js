import React from 'react';
import { shallow } from 'enzyme'
import SubHeading from '../SubHeading';

describe('SubHeading', () => {
    const wrapper = shallow(
        <SubHeading text="Test Heading" />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })
});