import React from 'react'
import { shallow } from 'enzyme'
import LoadingProgressIndicator from '../LoadingProgressIndicator'

describe('LoadingProgressIndicator', () => {
    const wrapper = shallow(
        <LoadingProgressIndicator
            id='testLoader'
            scale={2} />
    )
    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })
})