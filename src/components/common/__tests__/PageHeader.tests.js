import React from 'react'
import { shallow } from 'enzyme'
import PageHeader from '../PageHeader'

describe('PageHeader', () => {
    const wrapper = shallow(
        <PageHeader headerText='Test Header' />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })
})