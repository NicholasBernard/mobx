import React from 'react';
import ConfirmDeleteButtons from '../ConfirmDeleteButtons';
import { shallow } from 'enzyme';

describe('ConfirmDeleteButtons', () => {
    const onClickCancel = jest.fn();
    const onClickConfirm = jest.fn();
    const wrapper = shallow(
        <ConfirmDeleteButtons
            id={1}
            onClickCancel={onClickCancel}
            onClickConfirm={onClickConfirm} />
    );

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot();
    })
});