import React from 'react';
import { shallow } from 'enzyme';
import { ExternalNavButton, NavButton } from '../NavButton';
import toJson from 'enzyme-to-json';

const id = 'Testing';
const label = 'Test Label';
const link = '/someLink';

describe('NavButton', () => {
    const wrapper = shallow(
        <NavButton
            id={id}
            label={label}
            link={link} />
    );

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot();
    });
});

describe('ExternalNavButton', () => {
    const externalNavButtonWrapper = shallow(
        <ExternalNavButton
            id={id}
            label={label}
            link={link} />
    );

    it('should render correctly', () => {
        expect(externalNavButtonWrapper).toMatchSnapshot();
    });
});