import React from 'react';
import { shallow } from 'enzyme';
import NavButtons from '../NavButtons';

describe('NavButtons', () => {
    const wrapper = shallow(
        <NavButtons
            nextPage={'/nextPage'}
            onClick={jest.fn()}
            prevPage={'/prevPage'} />
    );

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot();
    });
});