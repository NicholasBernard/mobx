import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Button } from 'react-md'

export const NavButton = (props) => (
    <div>
        <Link to={props.link}>
            <Button                
                id={props.id}
                primary={props.primary}
                raised
                secondary={props.secondary}>
                {props.label}
            </Button>
        </Link>
    </div>
)

NavButton.propTypes = {
    id: PropTypes.string,
    label: PropTypes.string,
    link: PropTypes.string,
    primary: PropTypes.bool,
    secondary: PropTypes.bool
}

export const ExternalNavButton = (props) => (
    <div>
        <a href={props.link}>
            <Button
                id={props.id}
                primary={props.primary}
                raised
                secondary={props.secondary} >
                {props.label}
            </Button>
        </a>
    </div>
)

ExternalNavButton.propTypes = {
    id: PropTypes.string,
    label: PropTypes.string,
    link: PropTypes.string,
    primary: PropTypes.bool,
    secondary: PropTypes.bool
}