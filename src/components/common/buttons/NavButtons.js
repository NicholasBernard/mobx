import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Button } from 'react-md';

const NavButtons = (props) => (
    <div id='nav-buttons-container'>
        <div>
            <Link to={props.prevPage}>
                <Button                     
                    id={'prev-button'}
                    onClick={props.onClick}
                    raised
                    secondary>Previous</Button>
            </Link>
        </div>
        <div>
            <Link to={props.nextPage}>
                <Button                    
                    id={'next-button'}
                    onClick={props.onClick}
                    primary
                    raised>Next</Button>
            </Link>
        </div>
    </div>
)

NavButtons.propTypes = {
    prevPage: PropTypes.string,
    nextPage: PropTypes.string,
    onClick: PropTypes.func,
};

export default NavButtons;