import React from 'react';
import { Button } from 'react-md';
import PropTypes from 'prop-types';

const ConfirmDeleteButtons = (props) => (
    <div className='confirm-delete-buttons'>
        <Button
            id={'cancel-' + props.id}
            onClick={props.onClickCancel}
            raised
            secondary>
                Cancel
        </Button>
        <Button
            className='delete-button'
            id={'confirm-' + props.id}
            onClick={props.onClickConfirm}
            raised>
                Confirm
        </Button>
    </div>
)

ConfirmDeleteButtons.propTypes ={
    id: PropTypes.number,
    onClickCancel: PropTypes.func,
    onClickConfirm: PropTypes.func,
}

export default ConfirmDeleteButtons;