import React from 'react';
import PropTypes from 'prop-types';

const PageHeader = (props) => {
    return(
        <h1>{props.headerText}</h1>
    )
};

PageHeader.propTypes = {
    headerText: PropTypes.string,
}; 

export default PageHeader;