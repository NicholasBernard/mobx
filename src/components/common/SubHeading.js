import React from 'react';
import PropTypes from 'prop-types';

const SubHeading = (props) => {
    return(
        <h2>{props.text}</h2>
    )
};

SubHeading.propTypes = {
    text: PropTypes.string
};

export default SubHeading;