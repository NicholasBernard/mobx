import axios from 'axios'
import { action, observable } from 'mobx'
import { createESAuthToken, ROOT_URL } from '../../../services/baseService'

export class TechnicalContactStore {
    @observable emailAddress = ''
    @observable firstName = ''
    @observable lastName = ''
    @observable schoolId = ''
    @observable title = ''
    @observable objectToSave = {}
    @observable isDirty = false

    fetchData() {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}contacttechnical/get/`, createESAuthToken())
        .then(response => {this.setStore(response.data)})
    }

    saveData = () => {
        /* istanbul ignore next */
        if(this.isDirty){
            this.createObjectToSave()            
            axios.put(`${ROOT_URL}contacttechnical/save/`, this.objectToSave, createESAuthToken())
            this.resetStore()
        }
    }

    @action createObjectToSave = () => {
        const objectToSave = {
            firstName: this.firstName,
            lastName: this.lastName,
            emailAddress: this.emailAddress,
            schoolId: this.schoolId,
            title: this.title
        }

        this.objectToSave = objectToSave
    }

    @action resetStore = () => {
        this.isDirty = false
        this.objectToSave = {}
    }

    @action setStore = (data) => {
        this.emailAddress = data.emailAddress
        this.firstName = data.firstName
        this.lastName = data.lastName
        this.schoolId = data.schoolId
        this.title = data.title
    }

    @action updateStoreProperty = (e) => {
        this[e.target.name] = e.target.value
        this.isDirty = true
    }
}

export default new TechnicalContactStore()