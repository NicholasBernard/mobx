import React from 'react'
import { inject, observer } from 'mobx-react'
import { TextField } from 'react-md'
import PageHeader from '../../common/PageHeader'
import NavButtons from '../../common/buttons/NavButtons'

@inject('TechnicalContactStore')
@observer
export default class TechnicalContactDisplay extends React.Component {
    componentWillMount() {
        this.props.TechnicalContactStore.fetchData()
    }

    renderTextField = (label, name, value) => {
        return (
            <TextField               
                className="md-cell"
                floating={true}
                id={name + 'Input'} 
                label={label}
                name={name}
                onBlur={() => this.props.TechnicalContactStore.saveData()}
                onChange={(value, e) => this.props.TechnicalContactStore.updateStoreProperty(e)}
                value={value} />
        )
    }

    render() {
        const { emailAddress, firstName, lastName, title } = this.props.TechnicalContactStore
        return (
            <div>
                <PageHeader headerText='Set Technical Contact' />
                <p>Next we need to capture the information of your Technical Contact.</p>
                {this.renderTextField('First Name', 'firstName', firstName)}
                {this.renderTextField('Last Name', 'lastName', lastName)}
                {this.renderTextField('Email Address', 'emailAddress', emailAddress)}
                {this.renderTextField('Title', 'title', title)}                
                <NavButtons 
                    prevPage="/setAdmins"
                    nextPage="/sendWelcomeNotifications" />
            </div>
        )
    }
}