import React from 'react'
import { shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import TechnicalContactDisplay from '../TechnicalContactDisplay'

describe('TechnicalContactDisplay', () => {
    const store = {
        emailAddress: '',
        firstName: '',
        lastName: '',
        title: '',
        fetchData: jest.fn(),
        saveData: jest.fn(),
        updateStoreProperty: jest.fn()
    }
    const wrapper = shallow(
        <TechnicalContactDisplay.wrappedComponent
            TechnicalContactStore={store} />
    )
    
    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.fetchData).toHaveBeenCalledTimes(1)
    })

    it('calls the saveData function correctly onBlur of textfield', () => {
        wrapper.find('#firstNameInput').simulate('blur')
        expect(store.saveData).toHaveBeenCalledTimes(1)
    })

    it('calls the updateStateProperty function correctly onChange of textfield', () => {
        const firstNameEvent = { target: { name: 'firstName', value: 'Grigori'}}
        
        wrapper.find('#firstNameInput').simulate('change', firstNameEvent.target.value, firstNameEvent)
        expect(store.updateStoreProperty).toHaveBeenCalledTimes(1)
        expect(store.updateStoreProperty).toHaveBeenCalledWith(firstNameEvent)
    })
})