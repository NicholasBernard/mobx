import React from 'react'
import { TechnicalContactStore } from '../TechnicalContactStore'
import { initialState } from '../../../../testData/technicalContact'

describe('TechnicalContactStore', () => {
    const store = new TechnicalContactStore()

    it('updates objectToSave observable correctly on createSaveObject action', () => {
        store.emailAddress = initialState.emailAddress
        store.firstName = initialState.firstName
        store.lastName = initialState.lastName
        store.schoolId = initialState.schoolId
        store.title = initialState.title

        store.createObjectToSave()
        expect(store.objectToSave).toEqual({
            firstName: store.firstName,
            lastName: store.lastName,
            emailAddress: store.emailAddress,
            schoolId: store.schoolId,
            title: store.title
        })
    })

    it('updates observables correctly on resetStore action', () => {
        store.isDirty = true
        store.objectToSave = {
            firstName: store.firstName,
            lastName: store.lastName,
            emailAddress: store.emailAddress,
            schoolId: store.schoolId,
            title: store.title
        }
        
        store.resetStore()
        expect(store.isDirty).toBe(false)
        expect(store.objectToSave).toEqual({})
    })

    it('updates observables correctly on setStore action', () => {
        store.setStore(initialState)

        expect(store.emailAddress).toBe(initialState.emailAddress)
        expect(store.firstName).toBe(initialState.firstName)
        expect(store.lastName).toBe(initialState.lastName)
        expect(store.schoolId).toBe(initialState.schoolId)
        expect(store.title).toBe(initialState.title)
    })

    it('updates observables correctly on updateStoreProperty action', () => {
        const firstNameEvent = { target: { name: 'firstName', value: 'Grigori' }}
        const lastNameEvent = { target: { name: 'lastName', value: 'Rasputin' }}
        
        store.updateStoreProperty(firstNameEvent)
        store.updateStoreProperty(lastNameEvent)

        expect(store.firstName).toBe(firstNameEvent.target.value)
        expect(store.lastName).toBe(lastNameEvent.target.value)
    })
})