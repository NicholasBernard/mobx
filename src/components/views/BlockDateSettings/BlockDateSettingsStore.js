import axios from 'axios'
import { action, observable} from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class BlockDateSettingsStore {
    @observable onlyAdminsCanBlockDates = false
    @observable requireReasonWhenBlockingDates = false
    @observable objectToSave = {}

    fetchData() {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}blockdatesettings/get/`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    saveBlockDateSettings = (e) => {
        this.handleCheckEvent(e)
        this.createObjectToSave()
        /* istanbul ignore next */
        axios.put(`${ROOT_URL}blockdatesettings/save/`, this.objectToSave, createESAuthToken())
    }

    @action createObjectToSave = () => {
        this.objectToSave = {
            onlyAdminsCanBlockDates: this.onlyAdminsCanBlockDates,
            requireReasonWhenBlockingDates: this.requireReasonWhenBlockingDates
        }
    }

    @action handleCheckEvent = (e) => {
        this[e.target.name] = e.target.checked
    }

    @action setStoreItems = (data) => {
        this.onlyAdminsCanBlockDates = data.onlyAdminsCanBlockDates
        this.requireReasonWhenBlockingDates = data.requireReasonWhenBlockingDates
    }
}

export default new BlockDateSettingsStore()