import React from 'react'
import {inject, observer} from 'mobx-react'
import PageHeader from '../../common/PageHeader'
import NavButtons from '../../common/buttons/NavButtons'
import { SelectionControl } from 'react-md'

@inject('BlockDateSettingsStore')
@observer
export default class BlockDateSettingsDisplay extends React.Component{
    componentDidMount = () => {
        this.props.BlockDateSettingsStore.fetchData()
    }

    render(){
        const { onlyAdminsCanBlockDates, requireReasonWhenBlockingDates, saveBlockDateSettings} = this.props.BlockDateSettingsStore
        return(
            <div>
                <PageHeader headerText="Block Date Settings" />
                <p>Next we will set some permissions that are going to apply to your schools teacher when they are blocking a date.  An admin can change these permissions at any time after your data has been imported.</p>
                <p>First is whether or not you want any teacher in you school to be able to block dates.  If you check the following option, ONLY users marked as admins will be able to block a date.  If you leave the option unchecked, EVERY teacher in you school will have permission to block dates.</p>
                <SelectionControl
                    checked={onlyAdminsCanBlockDates}
                    id="allowAdminsBlockDates"
                    label="Only allow admins to block dates?"
                    name="onlyAdminsCanBlockDates"
                    onChange={(checked, e) => saveBlockDateSettings(e)}
                    type="checkbox" />
                <p>The final option is whether you will require a reason for blocking dates.  If the following option is checked, the teacher MUST provide a reason for the blocked date in order to save their data.  If left unchecked, any teacher with permissions can block a date without giving a reason.</p>
                <SelectionControl
                    checked={requireReasonWhenBlockingDates}
                    id="requireReason"
                    label="Require a reason when blocking dates?"
                    name="requireReasonWhenBlockingDates"
                    onChange={(checked, e) => saveBlockDateSettings(e)}
                    type="checkbox" />
                <NavButtons
                    prevPage="/schoolCalendarSettings"
                    nextPage="/graduationSettings" />
            </div>
        )
    }
}