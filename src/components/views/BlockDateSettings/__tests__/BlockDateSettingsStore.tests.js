import React from 'react'
import {BlockDateSettingsStore} from '../BlockDateSettingsStore'

describe('BlockDateSettingsStore', () => {
    const store = new BlockDateSettingsStore()
    store.onlyAdminsCanBlockDates = true
    store.requireReasonWhenBlockingDates = false

    it('calls functions correctly on the saveBlockDateSettings function', () => {
        const event = { target: { name: 'onlyAdminsCanBlockDates', checeked: true}}
        const handleCheckSpy = jest.spyOn(store, 'handleCheckEvent')
        const createobjectSpy = jest.spyOn(store, 'createObjectToSave')

        store.saveBlockDateSettings(event)

        expect(handleCheckSpy).toHaveBeenCalledTimes(1)
        expect(handleCheckSpy).toHaveBeenCalledWith(event)
        expect(createobjectSpy).toHaveBeenCalledTimes(1)
    })

    it('updates observable correctly on createSaveObject action', () => {
        store.createObjectToSave()
        expect(store.objectToSave).toEqual({
            onlyAdminsCanBlockDates: store.onlyAdminsCanBlockDates,
            requireReasonWhenBlockingDates: store.requireReasonWhenBlockingDates})
    })

    it('updates observable correctly on handleCheckEvent action', () => {
        const adminEvent = { target: { name: 'onlyAdminsCanBlockDates', checked: false}}
        const requireEvent = { target: { name: 'requireReasonWhenBlockingDates', checked: true}}

        store.handleCheckEvent(adminEvent)
        store.handleCheckEvent(requireEvent)

        expect(store.onlyAdminsCanBlockDates).toBe(adminEvent.target.checked)
        expect(store.requireReasonWhenBlockingDates).toBe(requireEvent.target.checked)
    })

    it('updates observable correctly on setStoreItems action', () => {
        const data = {
            onlyAdminsCanBlockDates: true,
            requireReasonWhenBlockingDates: true
        }
        store.setStoreItems(data)
        expect(store.onlyAdminsCanBlockDates).toEqual(data.onlyAdminsCanBlockDates)
        expect(store.requireReasonWhenBlockingDates).toEqual(data.requireReasonWhenBlockingDates)
    })
})