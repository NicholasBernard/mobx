import React from 'react'
import { shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import BlockDateSettingsDisplay from '../BlockDateSettingsDisplay'

describe('BlockDateSettingsDisplay', () => {
    const store = {
        onlyAdminsCanBlockDates: true,
        requireReasonWhenBlockingDates: false,
        fetchData: jest.fn(),
        saveBlockDateSettings: jest.fn()
    }

    const wrapper = shallow(        
        <BlockDateSettingsDisplay.wrappedComponent
            BlockDateSettingsStore={ store } />        
    )

    it('renders correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.fetchData).toHaveBeenCalledTimes(1)
    })

    it('calls the saveBlockDateSettings fucntion onChange of selection controls', () => {
        const fakeEvent = {target: { checked: false}}
        
        wrapper.find('#allowAdminsBlockDates').simulate('change', fakeEvent)
        expect(store.saveBlockDateSettings).toHaveBeenCalledTimes(1)

        wrapper.find('#requireReason').simulate('change', fakeEvent)
        expect(store.saveBlockDateSettings).toHaveBeenCalledTimes(2)
    })
})