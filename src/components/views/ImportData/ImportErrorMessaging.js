import React from 'react'
import { ExpansionList, ExpansionPanel } from 'react-md'
import ListOfErrors from './ListOfErrors'

export default class ImportErrorMessaging extends React.Component {
    state = {
        collapsed: true
    }

    toggleCollapsed = () => {
        this.setState({
            collapsed: !this.state.collapsed
        })
    }

    render() {
        return (
            <div className='errors-container'>
                <p>We've found some errors that need to fixed before we can import your data.</p>
                <ExpansionList className='md-cell md-cell--12'>
                    <ExpansionPanel
                        footer={null}
                        label='Errors'>
                        <ListOfErrors
                            errorMessages={this.props.errorMessages} />
                    </ExpansionPanel>
                </ExpansionList>
            </div>
        )
    }
}