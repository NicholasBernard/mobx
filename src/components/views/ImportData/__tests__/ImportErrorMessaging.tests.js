import React from 'react'
import { shallow } from 'enzyme'
import ImportErrorMessaging from '../ImportErrorMessaging'

describe('ImportErrorMessaging', () => {
    const wrapper = shallow(
        <ImportErrorMessaging />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })

    it('should set the state correctly when the toggleCollapsed function is called', () => {
        wrapper.setState({ collapsed: true })
            
        wrapper.instance().toggleCollapsed()
        expect(wrapper.instance().state.collapsed).toBe(false)
        
        wrapper.instance().toggleCollapsed()
        expect(wrapper.instance().state.collapsed).toBe(true)
    })
})