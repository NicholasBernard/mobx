import React from 'react'
import { shallow } from 'enzyme'
import ImportSuccessMessaging from '../ImportSuccessMessaging'

describe('ImportSuccessMessaging', () => {
    const wrapper = shallow(
        <ImportSuccessMessaging
            departmentCount={10}
            rosterCount={20}
            staffCount={100}
            studentCount={1000} />
    )
    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()  
    })
})