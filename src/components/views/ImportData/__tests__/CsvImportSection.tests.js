import React from 'react'
import { shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import CsvImportSection from '../CsvImportSection'

describe('CsvImportSection', () => {
    const store = {
        importWasSuccessful: false,
        initCSVImport: jest.fn(),
        isImporting: false       
    }

    const wrapper = shallow(
        <CsvImportSection.wrappedComponent
            ImportDataStore={store} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })

    it('should call the uploadJustFile function onClick of #upload', () => {
        const spy = jest.spyOn(wrapper.instance(), 'uploadJustFile')
        wrapper.find('#upload').simulate('click')
        expect(spy).toHaveBeenCalledTimes(1)
    })

    it('should render the LoadingProgressIndicator only when isImporting is true', () => {
        store.isImporting = true
        wrapper.instance().forceUpdate()
        wrapper.update()
        expect(wrapper.find('#csvImportProgress').exists()).toEqual(true)

        store.isImporting = false
        wrapper.instance().forceUpdate()
        wrapper.update()
        expect(wrapper.find('#csvImportProgress').exists()).toEqual(false)
    })

    it('should render the Import Success Message only when importWasSuccessful is true', () => {
        store.importWasSuccessful = true
        wrapper.instance().forceUpdate()
        wrapper.update()
        expect(wrapper.find('#success-message-container').exists()).toEqual(true)
        expect(wrapper.find('#success-message').text()).toEqual(`Your data has been imported!  Our records show that we've imported the following:`)

        store.importWasSuccessful = false
        wrapper.instance().forceUpdate()
        wrapper.update()
        expect(wrapper.find('#success-message-container').exists()).toEqual(false)
    })
})