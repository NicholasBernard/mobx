import React from 'react'
import { ImportDataStore } from '../ImportDataStore'
import { dataFromServerNoErrors } from '../../../../testData/importData'

describe('ImportDataStore', () => {
    const store = new ImportDataStore

    it('populates observables correctly on the setStoreAfterImport action', () => {
        store.setStoreAfterImport(dataFromServerNoErrors)

        expect(store.isImporting).toBe(false)
        expect(store.departmentCount).toEqual(dataFromServerNoErrors.importCounts.departmentCount)
        //expect(store.errorMessages).toEqual(dataFromServerNoErrors.errorMessages)
        expect(store.staffCount).toEqual(dataFromServerNoErrors.importCounts.staffCount)
        expect(store.studentCount).toEqual(dataFromServerNoErrors.importCounts.studentCount)
        expect(store.rosterCount).toEqual(dataFromServerNoErrors.importCounts.rosterCount)
        expect(store.importWasSuccessful).toEqual(dataFromServerNoErrors.importWasSuccessful)
    })

    it('populates observables correctly on the setStoreItems action', () => {
        store.setStoreItems(true)

        expect(store.schoolIsClever).toBe(true)
    })

    it('populates observables correctly on the toggleIsImporting action', () => {
        store.isImporting = false

        store.toggleIsImporting()
        expect(store.isImporting).toBe(true)

        store.toggleIsImporting()
        expect(store.isImporting).toBe(false)
    })
})