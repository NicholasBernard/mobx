import React from 'react'
import { shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import CleverImportSection from '../CleverImportSection'

describe('CleverImportSection', () => {
    const store = {
        importWasSuccessful: false,
        initCleverImport: jest.fn(),
        isImporting: false       
    }

    const wrapper = shallow(
        <CleverImportSection.wrappedComponent
            ImportDataStore={store} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })

    it('should call initCleverImport function onClick of #import-clever', () => {
        wrapper.find('#import-clever').simulate('click')
        expect(store.initCleverImport).toHaveBeenCalledTimes(1)
    })

    it('should render the LoadingProgressIndicator only when isImporting is true', () => {
        store.isImporting = true
        wrapper.instance().forceUpdate()
        wrapper.update()
        expect(wrapper.find('#cleverImportProgress').exists()).toEqual(true)

        store.isImporting = false
        wrapper.instance().forceUpdate()
        wrapper.update()
        expect(wrapper.find('#cleverImportProgress').exists()).toEqual(false)
    })

    it('should render the Import Success Message onle when importWasSuccessful is true', () => {
        store.importWasSuccessful = true
        wrapper.instance().forceUpdate()
        wrapper.update()
        expect(wrapper.find('#success-message-container').exists()).toEqual(true)
        expect(wrapper.find('#success-message').text()).toEqual(`Your Clever data has been imported!  Our records show that we've imported the following:`)

        store.importWasSuccessful = false
        wrapper.instance().forceUpdate()
        wrapper.update()
        expect(wrapper.find('#success-message-container').exists()).toEqual(false)
    })
})