import React from 'react'

const ImportSuccessMessaging = (props) => (
    <React.Fragment>
        <p>Department count: {props.departmentCount}</p>
        <p>Staff count: {props.staffCount}</p>
        <p>Student count: {props.studentCount}</p>
        <p>Roster count: {props.rosterCount}</p>
    </React.Fragment>
)

export default ImportSuccessMessaging