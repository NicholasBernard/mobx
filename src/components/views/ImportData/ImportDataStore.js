import axios from 'axios'
import { observable, action } from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class ImportDataStore {
    @observable isImporting = false
    @observable schoolIsClever = false

    @observable departmentCount = ''
    @observable errorMessages = []
    @observable rosterCount = ''
    @observable staffCount = ''
    @observable studentCount = ''
    @observable importWasSuccessful

    fetchData() {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}importdata/schoolisclever`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    initCleverImport = () => {
        /* istanbul ignore next */
        this.toggleIsImporting()
        /* istanbul ignore next */
        axios.post(`${ROOT_URL}clever/import`, null, createESAuthToken())
        .then(response => {this.setStoreAfterImport(response.data)})
    }

    initCSVImport(data) {
        /* istanbul ignore next */
        this.toggleIsImporting()
        /* istanbul ignore next */
        axios.post(`${ROOT_URL}uploader/uploadfiles`, data, createESAuthToken())
        .then(response => {this.setStoreAfterImport(response.data)})
    }

    @action setStoreAfterImport = (data) => {
        this.isImporting = false
        this.departmentCount = data.importCounts.departmentCount
        this.errorMessages = data.errorMessages
        this.staffCount = data.importCounts.staffCount
        this.studentCount = data.importCounts.studentCount
        this.rosterCount = data.importCounts.rosterCount
        this.importWasSuccessful = data.importWasSuccessful
    }

    @action setStoreItems = (data) => {
        this.schoolIsClever = data
    }

    @action toggleIsImporting = () => {
        this.isImporting = !this.isImporting
    }
}

export default new ImportDataStore()