import React from 'react'
import { Button, FileUpload, TextField } from 'react-md'
import { inject, observer } from 'mobx-react'
import LoadingProgressIndicator from '../../common/LoadingProgressIndicator'
import ImportSuccessMessaging from './ImportSuccessMessaging'
import ImportErrorMessaging from './ImportErrorMessaging'

@inject('ImportDataStore')
@observer
export default class CsvImportSection extends React.Component {
    constructor (props){
        super(props);
        this.state = {
            justFileServiceResponse: '',
            staffFile: '',
            studentFile: '',
            rosterFile: ''
        }
    }

    fileLoaded = (file, name) => {        
        this.setState({
            [name]: file.name
        })
    }

    uploadJustFile = (e) => {
        this.setState({
            justFileServiceResponse: 'Please wait'
        })

        if (!this.state.staffFile === '' || this.state.studentFile === '') {
            this.setState({
                justFileServiceResponse: 'First select a file!'
            });
            return
        }

        let form = new FormData();
        form.append('staffFile', document.getElementById("staffFile").files[0])
        form.append('studentFile', document.getElementById("studentFile").files[0])
        form.append('rosterFile', document.getElementById("rosterFile").files[0])

        this.props.ImportDataStore.initCSVImport(form)
    }

    render() {
        const { departmentCount, rosterCount, staffCount, studentCount} = this.props.ImportDataStore
        return(
            <div>
                <p>Now that we have determined these settings, we are ready to import your data files.  All files must be in a CSV form.  Let's go over each file, one at a time.</p>
                <p>First, we will select the Staff file.  This is a list of all staffers that will be using Enriching Students.  Please click the 'Choose Staff File' button and navigate to where that file is located.</p>
                <div className='file-input'>
                    <FileUpload
                        accept=".csv"
                        className="md-cell"
                        id="staffFile"              
                        label="Choose Staff File"
                        name='staffFile'
                        onLoad={(file) => this.fileLoaded(file, 'staffFile')}
                        primary={true} />
                    {
                        this.state.staffFile !== '' &&
                            <TextField
                                className='md-cell'
                                disabled            
                                id='staff-file'
                                label='Staff File'
                                value={this.state.staffFile} />
                    }
                </div>
                <p>Next, we will select the Student file.  This is a list of all students that will be scheduled in the Enriching Student application.  Please click the 'Choose Student FIle' button and navigate to where that file is located.</p>
                <div className='file-input'>
                    <FileUpload
                        accept=".csv"
                        className="md-cell" 
                        id="studentFile"
                        label="Choose Student File"
                        name='studentFile'
                        onLoad={(file) => this.fileLoaded(file, 'studentFile')}
                        primary={true} />
                    {
                        this.state.studentFile !== '' ?
                            <TextField
                                className='md-cell'
                                disabled            
                                id='student-file'
                                label='Student File'
                                value={this.state.studentFile} />
                        : null
                    }
                </div>
                <p>Finally, we  can select an optional Roster file.  This is file is optional, and if it's not provided, students will still be assigned to their homerooms.  However, no other Student to Teacher relationships will be created.  If you have a Student to Courses file ready, please select the 'Choose Student to Courses File' button below.</p>
                <div className='file-input'>
                    <FileUpload
                        accept=".csv"
                        className="md-cell" 
                        id="rosterFile"
                        label="Choose Roster File"
                        name='rosterFile'
                        onLoad={(file) => this.fileLoaded(file, 'rosterFile')}
                        primary={true} />
                    {
                        this.state.rosterFile !== '' ?
                            <TextField
                                className='md-cell'
                                disabled            
                                id='roster-file'
                                label='Roster File'
                                value={this.state.rosterFile} />
                        : null
                    }
                </div>
                <p>When you have finished selecting your files, please click the Upload button.</p>
                <Button
                    className='md-cell'
                    id='upload'     
                    onClick={(e) => this.uploadJustFile(e)}
                    primary
                    raised>
                        Upload
                </Button>
                {
                    this.props.ImportDataStore.isImporting &&
                    <LoadingProgressIndicator
                        id='csvImportProgress'
                        loadingMessage='Importing your data'
                        scale={2} />
                }
                {
                    this.props.ImportDataStore.importWasSuccessful &&
                    <div id='success-message-container'>
                        <p id='success-message'>Your data has been imported!  Our records show that we've imported the following:</p>
                        <ImportSuccessMessaging
                            departmentCount={departmentCount}
                            rosterCount={rosterCount}
                            staffCount={staffCount}
                            studentCount={studentCount} />
                    </div>
                }
                {
                    this.props.ImportDataStore.importWasSuccessful === false &&
                    <div>
                        <ImportErrorMessaging
                            errorMessages={this.props.ImportDataStore.errorMessages} />
                    </div>
                }
            </div>
        )
    }
}