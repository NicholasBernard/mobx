import React from 'react'
import { inject, observer } from 'mobx-react'
import NavButtons from '../../common/buttons/NavButtons'
import PageHeader from '../../common/PageHeader'
import CsvImportSection from './CsvImportSection'
import CleverImportSection from './CleverImportSection'

@inject('ImportDataStore')
@observer
export default class ImportDataDisplay extends React.Component {
    componentWillMount() {
        this.props.ImportDataStore.fetchData()
    }

    render(){
        return (
            <div>
                <PageHeader headerText="Import Data" />
                {
                    !this.props.ImportDataStore.schoolIsClever ?
                        <CsvImportSection />
                    :   <CleverImportSection />
                }                                
                <NavButtons
                    prevPage="/previewData"
                    nextPage="/setAdmins" />
            </div>
        )
    }
}