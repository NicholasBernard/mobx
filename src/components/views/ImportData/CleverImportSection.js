import React from 'react'
import { Button } from 'react-md'
import { inject, observer } from 'mobx-react'
import LoadingProgressIndicator from '../../common/LoadingProgressIndicator'
import ImportSuccessMessaging from './ImportSuccessMessaging'

@inject('ImportDataStore')
@observer
export default class CleverImportSection extends React.Component {
    render() {
        const { departmentCount, initCleverImport, rosterCount, staffCount, studentCount} = this.props.ImportDataStore
        return(
            <div>
                <p>We are now ready to import your data from Clever.  Please click the Import button to continue.</p>
                <Button 
                    raised
                    className='md-cell'
                    id='import-clever'
                    onClick={() => initCleverImport()}
                    primary
                    >
                        Import
                </Button>
                {
                    this.props.ImportDataStore.isImporting === true ?
                    <LoadingProgressIndicator
                        id='cleverImportProgress'
                        loadingMessage='Importing your data'
                        scale={2} />
                    : null
                }
                {
                    this.props.ImportDataStore.importWasSuccessful ?
                    <div id='success-message-container'>
                        <p id='success-message'>Your Clever data has been imported!  Our records show that we've imported the following:</p>
                        <ImportSuccessMessaging
                            departmentCount={departmentCount}
                            rosterCount={rosterCount}
                            staffCount={staffCount}
                            studentCount={studentCount} />
                    </div>
                    : null
                }      
            </div>
        )
    }
}