import React from 'react'
import {
    DataTable,
    TableBody,
    TableRow,
    TableColumn,
    TablePagination,
} from 'react-md'

export default class ListOfErrors extends React.Component {
    data = this.props.errorMessages.map((errorMessage, i) => ({
        key: i,
        error: errorMessage
    }))
    rows = this.data.length
    
    state = { slicedData: this.data.slice(0, 10)}

    handlePagination = (start, rowsPerPage) => {
        this.setState({ slicedData: this.data.slice(start, start + rowsPerPage)})
    }

    render() {
        const { slicedData } = this.state
        const rowsPerPageLabel = 'Rows per page'
        return (
            <DataTable baseId='errors-pagination'>
                <TableBody>
                    {slicedData.map(({ key, error }) => (
                        <TableRow key={key} selectable={false}>
                            <TableColumn className='error-message'>{error}</TableColumn>
                        </TableRow>
                    ))}
                </TableBody>
                <TablePagination rows={this.rows} rowsPerPageLabel={rowsPerPageLabel} onPagination={this.handlePagination} />
            </DataTable>
        )
    }
}