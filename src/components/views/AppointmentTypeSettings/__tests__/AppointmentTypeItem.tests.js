import React from 'react'
import AppointmentTypeItem from '../AppointmentTypeItem'
import { shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import { appointmentTypeFromForm } from '../../../../testData/appointmentTypeSettings'

describe('AppointmentTypeItem', () => {
    const store = {
        deleteAppointmentType: jest.fn(),
        saveAppointmentType: jest.fn(),
        saveOnBlur: jest.fn(),
        updateAppointmentType: jest.fn()
    }

    const wrapper = shallow(
        <AppointmentTypeItem.wrappedComponent
            AppointmentTypeSettingsStore={store}
            details={appointmentTypeFromForm} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })

    it('should set state correctly when the handleCancelClick function is called', () => {
        wrapper.setState({showConfirmDeleteOptions: true})
        wrapper.instance().handleCancelClick();
        expect(wrapper.instance().state.showConfirmDeleteOptions).toBe(false);
    });

    it('should set state correctly when the handleRemoveClick function is called', () => {
        wrapper.setState({showConfirmDeleteOptions: false})
        wrapper.instance().handleRemoveClick();
        expect(wrapper.instance().state.showConfirmDeleteOptions).toBe(true);
    });

    it('should set state correctly when the handleConfirmRemove function is called, and call deleteAppointmentType correctly', () => {
        wrapper.instance().handleConfirmRemove();

        expect(wrapper.instance().state.showConfirmDeleteOptions).toBe(false);
        expect(store.deleteAppointmentType).toHaveBeenCalledTimes(1);
    });

    it('should call the onBlurSave function onBlur of #appointment-type-description', () => {
        wrapper.find('#appointment-type-description-100').simulate('blur');
        expect(store.saveOnBlur).toHaveBeenCalledTimes(1);
    });

    it('should call the updateAppointmentType function onChange of #appointment-type-description', () => {
        wrapper.find('#appointment-type-description-100').simulate('change', {}, 0);
        expect(store.updateAppointmentType).toHaveBeenCalled();
    });

    it('should call the updateAppointmentType function onChange of #is-default', () => {
        wrapper.find('#is-default-100').simulate('change', {}, 0);
        expect(store.updateAppointmentType).toHaveBeenCalled();
    });

    it('should call the handleRemoveClick function on button click', () => {
        const spy = jest.spyOn(wrapper.instance(), 'handleRemoveClick');
        wrapper.setState({showConfirmDeleteOptions: false});
        wrapper.find('#remove-100').simulate('click');

        expect(spy).toHaveBeenCalledTimes(1);
    });
})