import React from 'react'
import {AppointmentTypeSettingsStore} from '../AppointmentTypeSettingsStore'
import { observable } from 'mobx'
import { appointmentTypeFromForm, appointmentTypeObservable, listOfAppointmentTypes, listOfAppointmentTypesAfterDelete } from '../../../../testData/appointmentTypeSettings'

describe('AppointmentTypeSettingsStore', () => {
    const store = new AppointmentTypeSettingsStore()
    const updateSpy = jest.spyOn(store, 'updateAppointmentType')
    const resetStoreSpy = jest.spyOn(store, 'resetStoreAfterSave')

    store.listOfAppointmentTypes = listOfAppointmentTypes

    it('calls the correct function from the saveAppointmentType function', () => {
        const testEvent = { target: { name: 'isDefault', checked: false}}

        store.saveAppointmentType(testEvent, 0)
        expect(updateSpy).toHaveBeenCalledTimes(1)
        expect(updateSpy).toHaveBeenCalledWith(testEvent, 0)
        expect(resetStoreSpy).toHaveBeenCalledTimes(1)
    })

    it('calls the correct functions after the saveOnBlur function is called', () => {
        store.isDirty = false
        store.saveOnBlur()
        expect(resetStoreSpy).toHaveBeenCalledTimes(1)

        store.isDirty = true
        store.saveOnBlur()
        expect(resetStoreSpy).toHaveBeenCalledTimes(2)
    })

    it('sets observable correctly on addNewAppointmentType action', () => {
        store.listOfAppointmentTypes = listOfAppointmentTypes
        store.addNewAppointmentType(appointmentTypeObservable)
        expect(store.listOfAppointmentTypes.length).toBe(4)
        expect(store.listOfAppointmentTypes[3]).toEqual(appointmentTypeObservable)
    })

    it('updates observables on resetStoreAfterSave action', () => {
        store.isDirty = true
        store.resetStoreAfterSave()
        expect(store.isDirty).toBe(false)
    })

    it('updates observables on resetStoreAfterDelete action', () => {
        store.listOfAppointmentTypes = listOfAppointmentTypes
        store.resetStoreAfterDelete(1)
        expect(store.listOfAppointmentTypes).toEqual(listOfAppointmentTypesAfterDelete)
    })

    it('updates observables on setStoreItems action', () => {
        store.listOfAppointmentTypes = []
        store.setStoreItems(listOfAppointmentTypes)
        expect(store.listOfAppointmentTypes).toEqual(listOfAppointmentTypes)
    })

    it('updates observables on updateAppointmentType action', () => {
        const isDefaultEvent = { target: { name: 'isDefault', checked: true}}
        const inputEvent = { target: { value: 'Edited Input Value'}}
        store.listOfAppointmentTypes = listOfAppointmentTypes

        store.updateAppointmentType(isDefaultEvent, 0)
        expect(store.listOfAppointmentTypes[0].isDefault).toBe(isDefaultEvent.target.checked)

        store.updateAppointmentType(inputEvent, 1)
        expect(store.listOfAppointmentTypes[1].description).toBe(inputEvent.target.value)
    })
})