import React from 'react'
import AppointmentTypeSettingsDisplay from '../AppointmentTypeSettingsDisplay';
import { shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import { listOfPeriods } from '../../../../testData/appointmentTypeSettings'

describe('AppointmentTypeSettingsDisplay', () => {
    const store = {
        fetchData: jest.fn(),
        listOfPeriods: listOfPeriods
    }

    const wrapper = shallow(
        <AppointmentTypeSettingsDisplay.wrappedComponent
            AppointmentTypeSettingsStore={store} />            
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.fetchData).toHaveBeenCalledTimes(1)
    })
})