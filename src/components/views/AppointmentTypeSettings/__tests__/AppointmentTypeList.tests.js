import React from 'react';
import { shallow } from 'enzyme';
import AppointmentTypeList from '../AppointmentTypeList';
import AppointmentTypeItem from '../AppointmentTypeItem';
import { listOfAppointmentTypes } from '../../../../testData/appointmentTypeSettings';

describe('AppointmentTypeList', () => {
    const wrapper = shallow(
        <AppointmentTypeList
            appointmentTypes={listOfAppointmentTypes} />
    );

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot();
        expect(wrapper.children().length).toBe(listOfAppointmentTypes.length);
        for(let i = 0; i < listOfAppointmentTypes.length; i++){
            expect(wrapper.childAt(i).type()).toBe(AppointmentTypeItem);
        };
    });
});