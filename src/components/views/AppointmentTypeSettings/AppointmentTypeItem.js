import React from 'react'
import ConfirmDeleteButtons from '../../common/buttons/ConfirmDeleteButtons'
import { Button, SelectionControl, TextField } from 'react-md'
import { inject, observer } from 'mobx-react'

@inject('AppointmentTypeSettingsStore')
@observer
export default class AppointmentTypeItem extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            showConfirmDeleteOptions: false
        }
    }

    handleCancelClick = () => {
        this.setState({
            showConfirmDeleteOptions: false
        })
    }

    handleRemoveClick = () => {
        this.setState({
            showConfirmDeleteOptions: true
        })
    }

    handleConfirmRemove = () => {
        this.setState({
            showConfirmDeleteOptions: false
        }, this.props.AppointmentTypeSettingsStore.deleteAppointmentType(this.props.details.appointmentTypeId, this.props.index))
    }

    render(){
        const { appointmentTypeId, description, isDefault } = this.props.details
        const { saveAppointmentType, saveOnBlur, updateAppointmentType } = this.props.AppointmentTypeSettingsStore
        return(        
            <div>
                <div className='appointment-type-item'>
                    <TextField 
                        className="md-cell md-cell--bottom"
                        id={"appointment-type-description-" + appointmentTypeId}
                        name="description"
                        onBlur={() => saveOnBlur()}
                        onChange={(value, e) => updateAppointmentType(e, this.props.index)}
                        value={description}
                        />                        
                    <SelectionControl 
                        checked={isDefault}
                        disabled={isDefault}
                        id={"is-default-" + appointmentTypeId}
                        label="Is Default"
                        labelBefore={true}
                        name="isDefault"
                        onChange={(value, e) => saveAppointmentType(e, this.props.index)}
                        type="checkbox" />
                    {
                        this.state.showConfirmDeleteOptions ?
                            <ConfirmDeleteButtons
                                id={appointmentTypeId}
                                onClickCancel={this.handleCancelClick}
                                onClickConfirm={this.handleConfirmRemove} />
                        :
                            <Button
                                className='delete-button'
                                disabled={isDefault}
                                id={'remove-' + appointmentTypeId}
                                onClick={() => this.handleRemoveClick()}
                                raised>
                                    Delete
                            </Button>
                    }                                            
                </div>        
            </div>
        )
    }
}