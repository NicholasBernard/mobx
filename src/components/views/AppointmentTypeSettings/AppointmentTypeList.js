import React from 'react'
import AppointmentTypeItem from './AppointmentTypeItem'

const AppointmentTypeList = (props) => (
    <div>
        {
            props.appointmentTypes.map((appointmentType, index) => <AppointmentTypeItem
                details={appointmentType} 
                key={index}
                index={index} />)
        }
    </div>
)

export default AppointmentTypeList