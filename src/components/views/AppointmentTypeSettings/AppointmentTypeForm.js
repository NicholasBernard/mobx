import React from 'react'
import { observer } from 'mobx-react'
import TextField from '../../common/formControls/TextField'
import Checkbox from '../../common/formControls/Checkbox'
import { Button } from 'react-md'

export default observer(({ form }) => (
    <form onSubmit={form.onSubmit} className='appointment-type-controls'>
        <TextField field={form.$('appointmentType')} />
        <Checkbox field={form.$('isDefault')} />
        <Button 
            primary
            raised
            onClick={form.onSubmit} >
                Add
        </Button>
        {/* <br />
        <button type='submit' onClick={form.onSubmit}>Submit</button>
        <button type='submit' onClick={form.onClear}>Clear</button>
        <button type='submit' onClick={form.onReset}>Reset</button> */}

        {/* Not sure what this tag is for, can't seem to generate form errors */}
        <p>{form.error}</p>
    </form>
))