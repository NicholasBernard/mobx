import axios from 'axios'
import { observable, action } from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class AppointmentTypeSettingsStore {
    @observable listOfAppointmentTypes = []
    @observable isDirty = false

    deleteAppointmentType(id, index) {
        /* istanbul ignore next */
        axios.delete(`${ROOT_URL}appointmenttypes/delete/${id}`, createESAuthToken()) 
        /* istanbul ignore next */
        this.resetStoreAfterDelete(index)       
    }

    fetchData() {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}appointmenttypes/get/`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    saveAppointmentType = (e, i) => {
        this.updateAppointmentType(e, i)
        /* istanbul ignore next */
        axios.put(`${ROOT_URL}appointmenttypes/save/`, this.listOfAppointmentTypes, createESAuthToken())
        this.resetStoreAfterSave()
    }

    saveNewAppointmentType = (appointmentType) => {
        /* istanbul ignore next */
        this.addNewAppointmentType(appointmentType)
        /* istanbul ignore next */
        axios.put(`${ROOT_URL}appointmenttypes/save/`, this.listOfAppointmentTypes, createESAuthToken()).then(() => this.fetchData()) // appointment type id gets created on server
    }

    saveOnBlur = () => {        
        if(this.isDirty){
            /* istanbul ignore next */
            axios.put(`${ROOT_URL}appointmenttypes/save/`, this.listOfAppointmentTypes, createESAuthToken())
            this.resetStoreAfterSave()
        }
    }

    @action addNewAppointmentType = (appointmentType) => {
        const newArray = this.listOfAppointmentTypes.slice()
        newArray.push(appointmentType)
        this.listOfAppointmentTypes = newArray
    }

    @action resetStoreAfterSave = () => {
        this.isDirty = false
    }

    @action resetStoreAfterDelete = (i) => {
        const newArray = [
            ...this.listOfAppointmentTypes.slice(0, i), //from the start to the index
            ...this.listOfAppointmentTypes.slice(i + 1) //from the delete to the end
        ]

        this.listOfAppointmentTypes = newArray
    }

    @action setStoreItems = (data) => {
        this.listOfAppointmentTypes = data
    }

    @action updateAppointmentType = (e, i) => {
        let newArray = this.listOfAppointmentTypes.slice()

        if(e.target.name === 'isDefault'){ //we know this is the checkbox
            newArray.map((item) => { //loop through all checkboxs and uncheck
                return item.isDefault = false                
            })
            newArray[i].isDefault = e.target.checked
        }else { //we are in the input
            newArray[i].description = e.target.value
            this.isDirty = true
        }

        this.listOfAppointmentTypes = newArray        
    }
}

export default new AppointmentTypeSettingsStore()