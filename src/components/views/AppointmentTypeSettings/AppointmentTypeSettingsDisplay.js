import React from 'react'
import { inject, observer } from 'mobx-react'
import NavButtons from '../../common/buttons/NavButtons'
import PageHeader from '../../common/PageHeader'
import SubHeading from '../../common/SubHeading'
import AppointmentTypeForm from './AppointmentTypeForm'
import AppointmentTypeList from './AppointmentTypeList'

import formRules from '../../../formRules'

@inject('AppointmentTypeSettingsStore')
@observer
export default class AppointmentTypeSettingsDisplay extends React.Component{
    componentDidMount = () => {
        this.props.AppointmentTypeSettingsStore.fetchData();
    }

    render(){
        return(
            <div>
                <PageHeader headerText="Appointment Type Settings" />
                <p>Now we are going to create a list of the different Appointment Types your school will use.  These will help determine what the course is, ie. Extra Help.  An Appointment Type marked as default will be the default option when scheduling.  This can be overwritten by any teacher on a course by course basis.</p>
                <p>Please note that an Appointment Type marked as 'Is Default', cannot be deleted.</p>
                <p>If you want to add more Appointment Types, click the 'Add' button.</p>
                <AppointmentTypeForm form={formRules} />
                <SubHeading text="Appointment Types" />
                <AppointmentTypeList
                    appointmentTypes={this.props.AppointmentTypeSettingsStore.listOfAppointmentTypes} />
                <NavButtons 
                    prevPage="/prebookSettingsPeriods"
                    nextPage="/schoolCalendarSettings" />
            </div>
        )
    }
}