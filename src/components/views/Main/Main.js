import React from 'react'
import { inject, observer } from 'mobx-react'
import { Route, Switch } from 'react-router-dom'
import { withRouter } from 'react-router'

import AppointmentTypeSettingsDisplay from '../AppointmentTypeSettings/AppointmentTypeSettingsDisplay'
import AttendanceSettingsDisplay from '../AttendanceSettings/AttendanceSettingsDisplay'
import BlockDateSettingsDisplay from '../BlockDateSettings/BlockDateSettingsDisplay'
import GraduationSettingsDisplay from '../GraduationSettings/GraduationSettingsDisplay'
import FinishDisplay from '../Finish/FinishDisplay'
import ImportDataDisplay from '../ImportData/ImportDataDisplay'
import ImportGradeSettingsDisplay from '../ImportGradeSettings/ImportGradeSettingsDisplay'
import PeriodNamesDisplay from '../PeriodNames/PeriodNamesDisplay'
import PeriodsPrefixDisplay from '../PeriodsPrefix/PeriodsPrefixDisplay'
import PrebookSettingsDaysDisplay from '../PrebookSettingsDays/PrebookSettingsDaysDisplay'
import PrebookSettingsPeriodsDisplay from '../PrebookSettingsPeriods/PrebookSettingsPeriodsDisplay'
import PreviewDataDisplay from '../PreviewData/PreviewDataDisplay'
import SchoolBrandingStudentSettingsDisplay from '../SchoolBrandingStudentSettings/SchoolBrandingStudentSettingsDisplay'
import SchoolCalendarSettingsDisplay from '../SchoolCalendarSettings/SchoolCalendarSettingsDisplay'
import SendWelcomeNotificationsDisplay from '../SendWelcomeNotifications/SendWelcomeNotificationsDisplay'
import SetAdminsDisplay from '../SetAdmins/SetAdminsDisplay'
import TechnicalContactDisplay from '../TechnicalContact/TechnicalContactDisplay'
import WelcomeDisplay from '../Welcome/WelcomeDisplay'

@withRouter
@inject('NavigationStore')
@observer
export default class Main extends React.Component {
    render() {
        return (
            <main>
            {
                this.props.NavigationStore.tokenIsBad ?
                <p>Token is bad</p>
                :
                <Switch>
                    <Route path='/finish' component={FinishDisplay} />
                    <Route path='/sendWelcomeNotifications' component={SendWelcomeNotificationsDisplay} />
                    <Route path='/setTechnicalContact' component={TechnicalContactDisplay} />
                    <Route path='/setAdmins' component={SetAdminsDisplay} />
                    <Route path='/importData' component={ImportDataDisplay} />
                    <Route path='/previewData' component={PreviewDataDisplay} />
                    <Route path='/importGradeSettings' component={ImportGradeSettingsDisplay} />
                    <Route path='/attendanceSettings' component={AttendanceSettingsDisplay} />
                    <Route path='/schoolBrandingStudentSettings' component={SchoolBrandingStudentSettingsDisplay} />
                    <Route path='/blockDateSettings' component={BlockDateSettingsDisplay} />
                    <Route path='/schoolCalendarSettings' component={SchoolCalendarSettingsDisplay} />
                    <Route path='/appointmentTypeSettings' component={AppointmentTypeSettingsDisplay} />
                    <Route path='/graduationSettings' component={GraduationSettingsDisplay} />
                    <Route path="/prebookSettingsPeriods" component={PrebookSettingsPeriodsDisplay} />
                    <Route path="/prebookSettings" component={PrebookSettingsDaysDisplay} />
                    <Route path='/periodNames' component={PeriodNamesDisplay} />
                    <Route path='/periodsPrefix' component={PeriodsPrefixDisplay} />
                    <Route path='/index' component={WelcomeDisplay} />
                    <Route path='/' component={WelcomeDisplay} />
                </Switch>
            }
            </main>
        )
    }
}