import React from 'react'
import { shallow } from 'enzyme'
import Navigation from '../Navigation'
import { wrappedComponent } from 'mobx'

describe('Navigation', () => {
    const store = {
        fetchSchoolName: jest.fn(),
        schoolName: 'Test School Name!',
        validateImportKey: jest.fn()
    }

    const wrapper = shallow(
        <Navigation.wrappedComponent
            NavigationStore={store} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.validateImportKey).toHaveBeenCalledTimes(1)
        expect(store.fetchSchoolName).toHaveBeenCalledTimes(1)
    })
})