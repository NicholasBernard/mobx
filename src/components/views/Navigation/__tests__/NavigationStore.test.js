import React from 'react'
import {NavigationStore} from '../NavigationStore'
import { wrappedComponent } from 'mobx'

describe('NavigationStore', () => {
    const store = new NavigationStore()
    
    it('sets observable correctly on setStoreItems action', () => {
        const testStore = {
            schoolName: 'Nics awesome test school!'
        }

        store.setStoreItems(testStore)
        
        expect(store.schoolName.schoolName).toBe(testStore.schoolName)
    })
})