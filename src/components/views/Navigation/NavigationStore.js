import axios from 'axios'
import { observable, action } from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class NavigationStore {
    @observable schoolName = ''
    @observable tokenIsBad = false

    validateImportKey(){
        //we need to get the import key from the url
        const importKey = window.location.search.substr(11);
        /* istanbul ignore next */
        if(importKey !== "")
        {
            localStorage.clear()
            /* istanbul ignore next */
            axios.get(`${ROOT_URL}authentication/byimportkey/${importKey}`)
            .then((response) =>{
              
              if(response.data === ''){
                this.setToken()
              }else{
                localStorage.setItem("esAuthToken", response.data)
                this.fetchSchoolName()
              } 
            })
        }
    }

    fetchSchoolName = () => {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}authentication/schoolname/`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    @action setStoreItems = (store) => {
        this.schoolName = store
    }

    @action setToken = () => {
        this.tokenIsBad = true
    }
}

export default new NavigationStore()