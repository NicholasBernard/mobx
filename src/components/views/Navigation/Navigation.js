import React from 'react'
import { inject, observer } from 'mobx-react'
import { NavLink } from 'react-router-dom'
import logo from '../../../images/logo.png'
import { withRouter } from 'react-router'

@withRouter
@inject('NavigationStore')
@observer
export default class Navigation extends React.Component {
    componentWillMount() {
        this.props.NavigationStore.validateImportKey()
    }

    render() {
        return(            
            <div>
                <nav>
                    <div className="logo">
                        <img alt='Enriching Students' src={logo} />
                    </div>
                    {
                        this.props.NavigationStore.tokenIsBad ? 
                        null
                        :
                        <React.Fragment>
                            <h2 className='school-name'>
                                {this.props.NavigationStore.schoolName}
                            </h2>
                            <ul>                            
                                <li><NavLink activeClassName='selected' to="/index">Welcome</NavLink></li>  
                                <li><NavLink activeClassName='selected' to="/periodsPrefix">Periods & Prefix</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/periodNames">Period Names</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/prebookSettings">Prebook Settings - Days</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/prebookSettingsPeriods">Prebook Settings - Periods</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/appointmentTypeSettings">Appointment Type Settings</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/schoolCalendarSettings">School Calendar Settings</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/blockDateSettings">Block Date Settings</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/graduationSettings">Graduation Setttings</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/schoolBrandingStudentSettings">School Branding & Student Settings</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/attendanceSettings">Attendance Settings</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/importGradeSettings">Import Grade Settings</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/previewData">Preview Your Data</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/importData">Import Data</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/setAdmins">Set Admins</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/setTechnicalContact">Set Technical Contact</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/sendWelcomeNotifications">Send Welcome Notifications</NavLink></li>
                                <li><NavLink activeClassName='selected' to="/finish">Finish</NavLink></li>
                            </ul>
                        </React.Fragment>
                    }                    
                </nav>                    
            </div>            
        )
    }
}