import axios from 'axios'
import { action, observable} from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class PrebookSettingsPeriodsStore {
    @observable listOfPeriodsByDay = []

    fetchData() {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}prebooksettingsperiods/get/`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    saveData = (checked, index, listIndex) => {
        this.updateSelectedPeriod(checked, index, listIndex)
        /* istanbul ignore next */
        axios.put(`${ROOT_URL}prebooksettingsperiods/save/`, this.listOfPeriodsByDay, createESAuthToken())
    }

    @action setStoreItems = (data) => {
        this.listOfPeriodsByDay = data
    }

    @action updateSelectedPeriod = (checked, index, listIndex) => {
        const newArray = this.listOfPeriodsByDay.slice()
        newArray[listIndex].schoolPeriods[index].isSelected = checked
        this.listOfPeriodsByDay = newArray
    }
}

export default new PrebookSettingsPeriodsStore()