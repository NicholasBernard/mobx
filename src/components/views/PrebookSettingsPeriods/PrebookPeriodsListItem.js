import React from 'react'
import { SelectionControl } from 'react-md'

const PrebookPeriodsListItem = (props) => (
    <div>
        {
            props.details.map((detail, index) => 
                <SelectionControl
                    checked={detail.isSelected}
                    key={index}
                    id={"period-" + props.listIndex + detail.periodId}
                    label={detail.periodDescription}
                    name="period-selector"
                    onChange={(checked, event) => props.saveData(checked, index, props.listIndex)}
                    type="checkbox" />
            )
        }
    </div>
)

export default PrebookPeriodsListItem