import React from 'react'
import { inject, observer } from 'mobx-react'
import PageHeader from '../../common/PageHeader'
import NavButtons from '../../common/buttons/NavButtons'
import PrebookPeriodsList from './PrebookPeriodsList'

@inject('PrebookSettingsPeriodsStore')
@observer
export default class PrebookSettingsPeriodsDisplay extends React.Component{
    componentDidMount() {
        this.props.PrebookSettingsPeriodsStore.fetchData()
    }

    render(){
        const {listOfPeriodsByDay, saveData } = this.props.PrebookSettingsPeriodsStore
        return(
            <div>
                <PageHeader headerText="Prebook Settings - Periods" />
                <p>Now that you have determined which days of the week you will be prebooking, it's time to decide which periods you want to prebook.</p>
                <PrebookPeriodsList
                    listOfPeriodsByDay={listOfPeriodsByDay}
                    saveData={saveData} />
                <NavButtons 
                    prevPage="/prebookSettings"
                    nextPage="/appointmentTypeSettings" />
            </div>
        )
    }
}