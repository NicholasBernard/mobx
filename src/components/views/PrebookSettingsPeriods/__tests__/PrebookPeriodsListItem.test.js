import React from 'react'
import PrebookPeriodsListItem from '../PrebookPeriodsListItem'
import { shallow } from 'enzyme'
import { details } from '../../../../testData/prebookSettingsPeriods'

describe('PrebookPeriodsListItem', () => {
    const saveData = jest.fn()
    const wrapper = shallow(
        <PrebookPeriodsListItem
            details={details}
            key={0}
            listIndex={0}
            saveData={saveData} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })

    it('should call savePrebookSettingsPeriods function onChange of selection control', () => {
        wrapper.find('#period-01').simulate('change', true, 0, 0)
        expect(saveData).toHaveBeenCalledTimes(1)
        expect(saveData).toHaveBeenCalledWith(true, 0, 0)
    })
})