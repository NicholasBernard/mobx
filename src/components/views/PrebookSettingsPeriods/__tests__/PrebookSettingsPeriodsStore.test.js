import React from 'react'
import {PrebookSettingsPeriodsStore} from '../PrebookSettingsPeriodsStore'

import {listOfPeriodsByDay} from '../../../../testData/prebookSettingsPeriods'

describe('PrebookSettingsPeriodsStore', () => {
    const store = new PrebookSettingsPeriodsStore()

    it('sets observables correctly on setStoreItems action', () => {        
        store.setStoreItems(listOfPeriodsByDay)
        expect(store.listOfPeriodsByDay).toEqual(listOfPeriodsByDay)
    })

    it('sets observables correctly on updateSelectedPeriod action', () => {
        store.listOfPeriodsByDay = listOfPeriodsByDay
        store.updateSelectedPeriod(false, 0, 0)
        store.updateSelectedPeriod(true, 1, 2)

        expect(store.listOfPeriodsByDay[0].schoolPeriods[0].isSelected).toBe(false)
        expect(store.listOfPeriodsByDay[2].schoolPeriods[1].isSelected).toBe(true)
    })

    it('should call correct action on saveData function', () => {
        const spy = jest.spyOn(store, 'updateSelectedPeriod')
        store.saveData(true, 0, 0)
        expect(spy).toHaveBeenCalledTimes(1)
        expect(spy).toHaveBeenCalledWith(true, 0, 0)
    })
})