import React from 'react'
import {shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import PrebookSettingsPeriodsDisplay from '../PrebookSettingsPeriodsDisplay'
import { listOfPeriodsByDay } from '../../../../testData/prebookSettingsPeriods'

describe('PrebookSettingsPeriodsDisplay', () => {
    const store = {
        fetchData: jest.fn()
    }

    const wrapper = shallow(
        <PrebookSettingsPeriodsDisplay.wrappedComponent
            listOfPeriodsByDay={listOfPeriodsByDay}
            PrebookSettingsPeriodsStore={store} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.fetchData).toHaveBeenCalledTimes(1)
    })    
})
