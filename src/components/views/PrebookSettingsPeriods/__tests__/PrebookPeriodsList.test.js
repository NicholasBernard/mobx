import React from 'react'
import PrebookPeriodsList from '../PrebookPeriodsList'
import { shallow } from 'enzyme'
import {observable} from 'mobx'
import { listOfPeriodsByDay } from '../../../../testData/prebookSettingsPeriods'

describe('PrebookPeriodsList', () => {
    const wrapper = shallow(
        <PrebookPeriodsList
            listOfPeriodsByDay={listOfPeriodsByDay} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })
})