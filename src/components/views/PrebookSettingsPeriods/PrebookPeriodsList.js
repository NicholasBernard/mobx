import React from 'react'
import PrebookPeriodsListItem from './PrebookPeriodsListItem'

const PrebookPeriodsList = (props) => {
    const returnWeekdayName = (weekdayId) => {
        switch (weekdayId) {
            case 1:
                return 'Monday'
            case 2:
                return 'Tuesday'
            case 3:
                return 'Wednesday'
            case 4:
                return 'Thursday'
            case 5:
                return 'Friday'
            default:
                return 'Loading...'
        }
    }

    return (
        <div>
            {
                props.listOfPeriodsByDay.map((item, index) =>
                    <div key={index}> 
                        <p id={'weekday-' + item.weekdayId}>Please select the period(s) you would like to block for {returnWeekdayName(item.weekdayId)}:</p>
                        <PrebookPeriodsListItem
                            details={item.schoolPeriods}
                            key={index}
                            listIndex={index}
                            saveData={props.saveData} />
                    </div>                 
                )        
            }
        </div>
    )
}

export default PrebookPeriodsList