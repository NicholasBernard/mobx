import React from 'react';
import PageHeader from '../../common/PageHeader';
import { ExternalNavButton, NavButton } from '../../common/buttons/NavButton';

export const FinishDisplay = () => (
    <div>
        <PageHeader headerText="Finish" />
        <p>You have completed setting up your schools accout within Enriching Students.  We hope that this process has been straight forward and easy.  If you have any further questions you can submit a support ticket from within the application.  We hope you have a great school year and we look forward to working with you.</p>
        <div id='nav-buttons-container'>
            <NavButton
                id='prev-button'
                label='Previous'
                link='/sendWelcomeNotifications'
                secondary={true} />
            <ExternalNavButton
                id='finish-button'
                label='finish'
                link='https://app.enrichingstudents.com/'
                primary={true} />
        </div>
    </div>
);

export default FinishDisplay