import React from 'react';
import { shallow } from 'enzyme';
import FinishDisplay from '../FinishDisplay';

describe('FinishDisplay', () => {
    const wrapper = shallow(
        <FinishDisplay />
    );

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot();
    });
});