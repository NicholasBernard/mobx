import React from 'react'
import { shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import { attendanceTypes } from '../../../../testData/attendanceSettings'
import AttendanceSettingsDisplay from '../AttendanceSettingsDisplay'

describe('AttendanceSettingsDisplay', () => {
    const store = {
        attendanceTypes: attendanceTypes,
        fetchData: jest.fn(),
        handleInputChange: jest.fn(),
        hideCutValue: true,
        saveAttendanceSettings: jest.fn(), 
        saveOnBlur: jest.fn()
    }

    const wrapper = shallow(        
        <AttendanceSettingsDisplay.wrappedComponent
            AttendanceSettingsStore={ store } />        
    )

    it('renders correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.fetchData).toHaveBeenCalledTimes(1)
    })

    it('should call saveAttendanceSettings onChange of #hide-cut-value', () => {
        wrapper.find('#hide-cut-value').simulate('change')
        expect(store.saveAttendanceSettings).toHaveBeenCalledTimes(1)
    })
})