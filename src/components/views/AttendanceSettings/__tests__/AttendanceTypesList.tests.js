import React from 'react';
import AttendanceTypesList from '../AttendanceTypesList';
import { shallow } from 'enzyme';
import { attendanceTypes } from '../../../../testData/attendanceSettings';

describe('AttendanceTypesList', () => {
    const handleInputChange = jest.fn();
    const saveOnBlur = jest.fn();

    const wrapper = shallow(
        <AttendanceTypesList
            attendanceTypes={attendanceTypes}
            handleInputChange={handleInputChange}
            saveOnBlur={saveOnBlur} />
    );

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot();
    });

    it('should call handleInputChange function when onChange occurs', () => {
        wrapper.find('#type-1').simulate('change', {target: { value: 'Testing'}});
        expect(handleInputChange).toHaveBeenCalled();
        expect(handleInputChange).toHaveBeenCalledTimes(1);
    });

    it('should call saveOnBlur function correctly', () => {
        wrapper.find('#type-1').simulate('blur');
        expect(saveOnBlur).toHaveBeenCalledTimes(1);
    })
});