import React from 'react'
import {AttendanceSettingsStore} from '../AttendanceSettingsStore'
import { attendanceTypes, attendanceTypesObservable } from '../../../../testData/attendanceSettings'

describe('AttendanceSettingsStore', () => {
    const store = new AttendanceSettingsStore()
    store.attendanceTypes = attendanceTypes
    store.hideCut = false

    it('calls the correct functions when saveAttendanceSettings function is called', () => {
        const cutValueSpy = jest.spyOn(store, 'handleCutValueChange')
        const createObjectSpy = jest.spyOn(store, 'createObjectToSave')
        const value = 'true'
        
        store.saveAttendanceSettings(value)
        expect(cutValueSpy).toHaveBeenCalledTimes(1)
        expect(cutValueSpy).toHaveBeenCalledWith(value)
        expect(createObjectSpy).toHaveBeenCalledTimes(1)
    })

    it('sets observable correctly on createSaveObject action', () => {
        store.createObjectToSave()
        expect(store.objectToSave).toEqual({
            attendanceTypes: store.attendanceTypes,
            hideCut: store.hideCut
        })
    })

    it('sets observable correctly on handleCutValueChange action', () => {
        store.handleCutValueChange(true)
        expect(store.hideCut).toBe(true)
    })

    it('sets observable correctly on handleInputChange action', () => {
        const event = { target: { value: 'Big Time Test Man!'}}

        store.handleInputChange(event, 0)
        expect(store.attendanceTypes[0].description).toBe(event.target.value)
    })

    it('sets observable correctly on setStoreAfterSave action', () => {
        store.isDirty = true
        store.objectToSave = {
            value: 'this will be deleted!'
        }

        store.resetStoreAfterSave()
        expect(store.isDirty).toBe(false)
        expect(store.objectToSave).toEqual({})
    })

    it('sets observable correctly on setStoreItems action', () => {
        store.attendanceTypes = {}
        store.hideCut = false

        const data = {
            attendanceTypes: attendanceTypes,
            hideCut: true
        }

        store.setStoreItems(data)
        expect(store.attendanceTypes).toEqual(attendanceTypesObservable)
        expect(store.hideCut).toEqual(data.hideCut)
    })
})