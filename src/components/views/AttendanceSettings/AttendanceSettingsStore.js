import axios from 'axios'
import { action, observable } from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class AttendanceSettingsStore {
    @observable attendanceTypes = []
    @observable hideCut = false
    @observable isDirty = false
    @observable objectToSave = {}
    
    fetchData() {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}attendancetypes/get/`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    saveAttendanceSettings = (value) => {
        this.handleCutValueChange(value)
        this.createObjectToSave()
        /* istanbul ignore next */
        axios.put(`${ROOT_URL}attendancetypes/save/`, this.objectToSave, createESAuthToken())
    }

    saveOnBlur = () => {
        /* istanbul ignore next */
        if(this.isDirty){
            this.createObjectToSave()
            axios.put(`${ROOT_URL}attendancetypes/save/`, this.objectToSave, createESAuthToken())
            this.resetStoreAfterSave()
        }        
    }

    @action createObjectToSave = () => {
        this.objectToSave = {
            attendanceTypes: this.attendanceTypes,
            hideCut: this.hideCut
        }
    }

    @action handleCutValueChange = (value) => {
        this.hideCut = value
    }

    @action handleInputChange = (e, i) => {
        const newArray = this.attendanceTypes.slice()
        newArray[i].description = e.target.value
        this.attendanceTypes = newArray
        this.isDirty = true
    }

    @action resetStoreAfterSave = () => {
        this.isDirty = false
        this.objectToSave = {}
    }

    @action setStoreItems = (data) => {
        this.attendanceTypes = data.attendanceTypes
        this.hideCut = data.hideCut
    }
}

export default new AttendanceSettingsStore()