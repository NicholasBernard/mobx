import React from 'react'
import { inject, observer } from 'mobx-react'
import PageHeader from '../../common/PageHeader'
import NavButtons from '../../common/buttons/NavButtons'
import { SelectionControl } from 'react-md'
import AttendanceTypesList from './AttendanceTypesList'

@inject('AttendanceSettingsStore')
@observer
export default class AttendanceSettingsDisplay extends React.Component{
    componentDidMount = () => {
        this.props.AttendanceSettingsStore.fetchData()
    }

    render(){
        const { attendanceTypes, handleInputChange, hideCut, saveAttendanceSettings, saveOnBlur } = this.props.AttendanceSettingsStore
        return(
            <div>
                <PageHeader headerText="Attendance Settings" />
                <p>Next we are going to setup the values that your teachers will use to take attendance.  These can be overwritten by an Admin within Enriching Students.</p>
                <AttendanceTypesList
                    attendanceTypes={attendanceTypes}
                    handleInputChange={handleInputChange}
                    saveOnBlur={saveOnBlur} />
                <p>Some schools prefer not to have the 'Cut' value display for teachers taking attendance.  If you check this permission, that vaule will not be displayed.  An Admin will still be able to mark students as 'Cut' on the Absent Students page within Enriching Students.</p>
                <SelectionControl
                    checked={hideCut}
                    className="md-cell"
                    id="hide-cut-value" 
                    label="Hide cut value from teacher when taking attendance?"
                    labelBefore={true}
                    name="hideCutValue"
                    onChange={(value, event) => saveAttendanceSettings(value)}
                    type="checkbox" />
                <NavButtons 
                    prevPage="/schoolBrandingStudentSettings"
                    nextPage="/importGradeSettings" />
            </div>
        )
    }
}