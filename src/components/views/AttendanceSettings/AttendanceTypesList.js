import React from 'react'
import { TextField } from 'react-md'

const AttendanceTypesList = (props) => (
    <div>
        {
            props.attendanceTypes.map((attendanceType, index) => 
                <div key={index}>
                    <p>{attendanceType.typeDescription}</p>
                    <TextField
                        className="md-cell"
                        id={'type-' + attendanceType.attendanceTypeId.toString()}
                        label={attendanceType.exportValue}
                        name="attendance-type"
                        onBlur={() => props.saveOnBlur()}
                        onChange={(value, event) => props.handleInputChange(event, index)}
                        value={attendanceType.description} />
                </div>
            )
        }
    </div>
)

export default AttendanceTypesList