import React from 'react';
import { TextField, SelectionControl } from 'react-md';

const StaffersListItem = (props) => (
    <div className="list-of-teachers">
        <TextField
            className="md-cell"
            defaultValue={props.details.lastName + ', ' + props.details.firstName}
            disabled={true}
            id={'textfield-' + props.details.userId}
            type="text" />
        <SelectionControl
            aria-label="Is admin"
            checked={props.details.userIsAdmin}
            id={'checkbox-' + props.details.userId}
            name="userIsAdmin" 
            onChange={(checked, e) => props.saveSetAdmins(e, props.index)}
            type="checkbox" />
        <SelectionControl
            aria-label="Is active"
            checked={props.details.userIsActive}
            id={'isActive-' + props.details.userId}
            name="userIsActive" 
            onChange={(checked, e) => props.saveSetAdmins(e, props.index)}
            type="checkbox" />
        <TextField
            className="md-cell"
            defaultValue={props.details.homeroom}
            onBlur={() => props.saveRoomNumber(props.index)}
            onChange={(value, event) => props.handleRoomNumberChange(value, props.index)}
            id={'homeroom-' + props.details.userId}
            type="text" />
    </div>
);

export default StaffersListItem;