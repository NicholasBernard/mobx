import React from 'react';
import StaffersListItem from './StaffersListItem';
import { SelectionControl } from 'react-md';

export default class StaffersList extends React.Component {
    renderLicenseInfo() {
        const className = `${this.props.numberOfActiveUsers > this.props.numberOfLicenses ? 'error-text' : 'no-error'}`

        return (
            <div>
                <p>
                    Licenses Purchased: {this.props.numberOfLicenses} / <span className={className}>Licenses Used: {this.props.numberOfActiveUsers}</span>
                </p>
            </div>
        )
    }    

    render() {
        return (
            <div>
                <div>
                    <p>Please note, as you mark a user as active it will use one of the licenses that your school has purchased.  You are currently using {this.props.numberOfActiveUsers} licenses of the {this.props.numberOfLicenses} total you've purchased.</p>
                </div>
                {this.renderLicenseInfo()}
                <div>
                    <SelectionControl
                        className='md-cell'
                        id='all-is-active'
                        label='Mark all Is Active'
                        labelBefore={true}
                        name='all-is-active'
                        onChange={(value, e) => this.props.saveSetAdmins(e)}
                        type='checkbox'/>
                </div>
                <div>
                    <ul className="list-subheadings">
                        <li>Teacher Name</li>
                        <li>Is Admin</li>
                        <li>Is Active</li>
                        <li>Room Number</li>
                    </ul>
                </div>        
                <div className='list-of-teacher-info'>
                    {
                        this.props.staffers.map((teacher, index) => 
                            <StaffersListItem
                                details={teacher}
                                key={index}
                                index={index}
                                handleInputChange={this.props.handleInputChange}
                                handleRoomNumberChange={this.props.handleRoomNumberChange}
                                saveSetAdmins={this.props.saveSetAdmins}
                                saveRoomNumber={this.props.saveRoomNumber} />
                        )
                    }
                </div>
            </div>
        )
    }
}