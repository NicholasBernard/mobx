import React from 'react';
import { inject, observer } from 'mobx-react'
import PageHeader from '../../common/PageHeader'
import NavButtons from '../../common/buttons/NavButtons'
import StaffersList from './StaffersList'

@inject('SetAdminsStore')
@observer
export default class SetAdminsDisplay extends React.Component{
    componentWillMount() {
        this.props.SetAdminsStore.fetchData();
    };

    render(){
        const { handleAllActiveChange, handleInputChange, handleRoomNumberChange, numberOfActiveUsers, numberOfLicenses, saveSetAdmins, saveRoomNumber, staffers } = this.props.SetAdminsStore
        return(            
            <div>
                <PageHeader headerText="Set Admins" />
                <p>Great!  Your data has been imported and we can now set Staff members to be marked as Admins within Enriching Students.  Staffers marked as Admins will have access to more permissions and pages then other Staff members.  Additionally, Staff members can be marked as Admins from within Enriching Students itself.</p>
                <StaffersList
                    handleAllActiveChange={handleAllActiveChange}
                    handleInputChange={handleInputChange}
                    handleRoomNumberChange={handleRoomNumberChange}
                    numberOfActiveUsers={numberOfActiveUsers}
                    numberOfLicenses={numberOfLicenses}
                    saveSetAdmins={saveSetAdmins}
                    saveRoomNumber={saveRoomNumber}
                    staffers={staffers} />
                <NavButtons 
                    prevPage="/importData"
                    nextPage="/setTechnicalContact" />
            </div>
        );
    }
}