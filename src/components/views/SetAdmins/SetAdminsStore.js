import axios from 'axios'
import { observable, action } from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class SetAdminsStore {
    @observable numberOfLicenses = ''
    @observable staffers = []
    @observable numberOfActiveUsers = 0
    @observable objectToSave = {}
    @observable isDirty = false

    fetchData() {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}staffers/get/`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    saveRoomNumber = (index) => {
        /* istanbul ignore next */
        if(this.isDirty){
            this.createObjectToSave(index)
            /* istanbul ignore next */
            axios.put(`${ROOT_URL}staffers/saveHomeroom/`, this.objectToSave, createESAuthToken())
            this.resetStoreAfterSave()
        }        
    }

    saveSetAdmins = (e, i) => {
        if(e.target.name === 'all-is-active'){
            this.handleAllActiveChange(e.target.checked)
        }else{
            this.handleInputChange(e, i)
        }        
        /* istanbul ignore next */
        axios.put(`${ROOT_URL}staffers/save/`, this.staffers, createESAuthToken())
    }

    @action adjustIsActiveCount = (e) => {
        if(e.target.checked === true){
            this.numberOfActiveUsers ++
        }else{
            this.numberOfActiveUsers --
        }
    }

    @action createObjectToSave = (index) => {
        const objectToSave = {
            homeroom: this.staffers[index].homeroom,
            userId: this.staffers[index].userId
        }

        this.objectToSave = objectToSave
    }

    @action handleAllActiveChange = (checked) => {
        if(checked === true){ //adjust count to max
            this.numberOfActiveUsers = this.staffers.length
        }else{
            this.numberOfActiveUsers = 0
        }

        const newArray = this.staffers.slice()
        newArray.map((staffer) => {
            return staffer.userIsActive = checked
        })

        this.staffers = newArray
    } 

    @action handleInputChange = (e, i) => {
        const newArray = this.staffers.slice()

        if(e.target.name === 'userIsActive'){
            this.adjustIsActiveCount(e)

            if(e.target.checked === false) {
                newArray[i].userIsAdmin = false
                this.staffers = newArray
            }
        }

        newArray[i][e.target.name] = e.target.checked

        this.staffers = newArray
    }

    @action handleRoomNumberChange = (value, index) => {
        const newArray = this.staffers.slice()
        newArray[index].homeroom = value

        this.staffers = newArray
        this.isDirty = true
    }

    @action resetStoreAfterSave = () => {
        this.isDirty = false
        this.objectToSave = {}
    }

    @action setStoreItems = (data) => {
        this.staffers = data.staffers
        this.numberOfLicenses = data.numberOfLicenses
        for(let i = 0; i < this.staffers.length; i++){
            if(this.staffers[i].userIsActive === true){
                this.numberOfActiveUsers ++
            }
        }
    }
}

export default new SetAdminsStore()