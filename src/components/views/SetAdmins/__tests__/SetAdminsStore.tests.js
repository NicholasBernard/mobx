import React from 'react'
import {SetAdminsStore} from '../SetAdminsStore'
import { staffers } from '../../../../testData/setAdmins'

describe('SetAdminsStore', () => {
    const store = new SetAdminsStore()
    store.staffers = staffers

    it('fires correct function from saveSetAdmins action, based on name', () => {
        const allActiveEvent = { target: { name: 'all-is-active', checked: true}}
        const userActiveEvent = { target: { name: 'userIsActive', checked: true}}
        
        const handleAllSpy = jest.spyOn(store, 'handleAllActiveChange')
        const handleInputSpy = jest.spyOn(store, 'handleInputChange')

        store.saveSetAdmins(allActiveEvent, 0)
        expect(handleAllSpy).toHaveBeenCalledTimes(1)
        expect(handleAllSpy).toHaveBeenCalledWith(allActiveEvent.target.checked)
        expect(handleInputSpy).toHaveBeenCalledTimes(0)

        store.saveSetAdmins(userActiveEvent, 0)
        expect(handleAllSpy).toHaveBeenCalledTimes(1)
        expect(handleInputSpy).toHaveBeenCalledTimes(1)
        expect(handleInputSpy).toHaveBeenCalledWith(userActiveEvent, 0)

    })

    it('updates observables correctly on adjustIsActive action', () => {
        store.numberOfActiveUsers = 10
        const trueEvent = { target: { checked: true}}
        const falseEvent = { target: { checked: false}}

        store.adjustIsActiveCount(trueEvent)
        expect(store.numberOfActiveUsers).toBe(11)

        store.adjustIsActiveCount(falseEvent)
        expect(store.numberOfActiveUsers).toBe(10)
    })

    it('updates observables correctly on createObjectToSave action', () => {
        store.objectToSave = {}
        store.staffers = staffers

        store.createObjectToSave(0)

        expect(store.objectToSave).toEqual({
            homeroom: store.staffers[0].homeroom,
            userId: store.staffers[0].userId
        })
    })

    it('updates observables correctly on handleAllActiveChange action', () => {
        store.staffers = staffers

        store.handleAllActiveChange(true)
        expect(store.numberOfActiveUsers).toEqual(store.staffers.length)
        for(let i = 0; i<store.staffers.length; i++){
            expect(store.staffers[i].userIsActive).toBe(true)
        }

        store.handleAllActiveChange(false)
        expect(store.numberOfActiveUsers).toEqual(0)
        for(let i = 0; i<store.staffers.length; i++){
            expect(store.staffers[i].userIsActive).toBe(false)
        }
    })

    it('updates observables correctly on handleInputChange action', () => {
        store.staffers = staffers

        const isActiveEvent = { target: { name: 'userIsActive', checked: true}}
        const isAdminEvent = { target: { name: 'userIsAdmin', checked: false}}

        const spy = jest.spyOn(store, 'adjustIsActiveCount')

        store.handleInputChange(isActiveEvent, 0)
        expect(spy).toHaveBeenCalledTimes(1)
        expect(store.staffers[0].userIsActive).toBe(isActiveEvent.target.checked)

        store.handleInputChange(isAdminEvent, 0)
        expect(store.staffers[0].userIsAdmin).toBe(isAdminEvent.target.checked)
    })

    it('deselects a user as admin if isActive is false', () => {
        store.staffers = staffers

        const isActiveEvent = { target: { name: 'userIsActive', checked: false}}
        const spy = jest.spyOn(store, 'adjustIsActiveCount')

        store.handleInputChange(isActiveEvent, 0)
        //expect(spy).toHaveBeenCalledTimes(1)
        expect(spy).toHaveBeenCalledWith(isActiveEvent)
        expect(store.staffers[0].userIsActive).toBe(false)
        expect(store.staffers[0].userIsAdmin).toBe(false)
    })

    it('updates observables correctly on handleRoomNumberChange action', () => {
        store.staffers = staffers
        const value = '101'
        const index = 1

        store.handleRoomNumberChange(value, index)
        expect(store.staffers[index].homeroom).toBe(value)
    })

    it('updates observables correctly on resetStoreAfterSave action', () => {
        store.isDirty = true
        store.objectToSave = {
            thisStuff: 'should be reset'
        }

        store.resetStoreAfterSave()
        expect(store.isDirty).toBe(false)
        expect(store.objectToSave).toEqual({})
    })

    it('updates observables correctly on setStoreItems action', () => {
        store.staffers = {}
        store.numberOfLicenses = 0

        const data = {
            staffers: staffers,
            numberOfLicenses: 55
        }

        store.setStoreItems(data)
        expect(store.staffers).toEqual(data.staffers)
        expect(store.numberOfLicenses).toEqual(data.numberOfLicenses)
    })
})