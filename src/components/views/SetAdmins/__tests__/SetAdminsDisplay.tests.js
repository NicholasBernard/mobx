import React from 'react'
import { shallow, mount } from 'enzyme'
import { wrappedComponent } from 'mobx'
import SetAdminsDisplay from '../SetAdminsDisplay'

describe('SetAdminsDisplay', () => {
    const store = {
        fetchData: jest.fn()
    }

    const wrapper = shallow(        
        <SetAdminsDisplay.wrappedComponent
            SetAdminsStore={ store } />        
    )

    it('renders correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.fetchData).toHaveBeenCalledTimes(1)
    })
})