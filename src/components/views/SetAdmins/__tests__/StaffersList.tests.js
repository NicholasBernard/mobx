import React from 'react';
import { shallow } from 'enzyme';
import StaffersList from '../StaffersList';
import { staffers } from '../../../../testData/setAdmins';

describe('StaffersList', () => {
    let numberOfActiveUsers = 10
    let numberOfLicenses = 20
    const saveSetAdmins = jest.fn();
    const wrapper = shallow(
        <StaffersList
            numberOfActiveUsers={numberOfActiveUsers}
            numberOfLicenses={numberOfLicenses}
            saveSetAdmins={saveSetAdmins}
            staffers={staffers} />
    );
    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot();
    });

    it('should render correct className based on number of users and licenses', () => {
        expect(wrapper.find('.no-error').length).toEqual(1)
        expect(wrapper.find('.error-text').length).toEqual(0)

        const wrapper2 = shallow(
            <StaffersList
                numberOfActiveUsers={50}
                numberOfLicenses={numberOfLicenses}
                saveSetAdmins={saveSetAdmins}
                staffers={staffers} />
        );
        
        expect(wrapper2.find('.no-error').length).toEqual(0)
        expect(wrapper2.find('.error-text').length).toEqual(1)
        
    })

    it('should call saveSetAdmins function on change of #all-is-active', () => {
        wrapper.find('#all-is-active').simulate('change');
        expect(saveSetAdmins).toHaveBeenCalledTimes(1);
    })
});