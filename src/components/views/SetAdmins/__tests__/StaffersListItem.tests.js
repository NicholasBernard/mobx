import React from 'react'
import { shallow } from 'enzyme'
import StaffersListItem from '../StaffersListItem'
import { details } from '../../../../testData/setAdmins'

describe('StaffersListItem', () => {
    const handleRoomNumberChange = jest.fn()
    const saveSetAdmins = jest.fn()
    const saveRoomNumber = jest.fn()
    const wrapper = shallow(
        <StaffersListItem
            details={details}
            handleRoomNumberChange={handleRoomNumberChange}
            saveSetAdmins={saveSetAdmins}
            saveRoomNumber={saveRoomNumber} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })

    it('should call updateTeacherInfo function onChange of isAdmin checkbox', () => {
        wrapper.find('#checkbox-' + details.userId).simulate('change', true, 0)
        expect(saveSetAdmins).toHaveBeenCalledTimes(1)
    })

    it('should call updateTeacherInfo function onChange of isActive checkbox', () => {
        wrapper.find('#isActive-' + details.userId).simulate('change', true, 0)
        expect(saveSetAdmins).toHaveBeenCalledTimes(2)
    })

    it('should call saveRoomNumber function onBlur of homeroom textfield', () => {
        wrapper.find('#homeroom-' + details.userId).simulate('blur')
        expect(saveRoomNumber).toHaveBeenCalledTimes(1)
    })

    it('should call handleRoomNumberChange function onChange of homeroom textfield', () => {
        wrapper.find('#homeroom-' + details.userId).simulate('change', 'TestValue', 0)
        expect(handleRoomNumberChange).toHaveBeenCalledTimes(1)
    })
})