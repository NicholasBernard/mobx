import axios from 'axios'
import { observable, action } from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class PeriodNamesStore {
    @observable listOfPeriods = []
    @observable isDirty = false

    fetchData = () => {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}schoolperiods/get/`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    saveData = () => {
        /* istanbul ignore next */
        if(this.isDirty){            
            axios.put(`${ROOT_URL}schoolperiods/save/`, this.listOfPeriods, createESAuthToken())
            this.resetStateAfterSave()
        }  
    }

    @action resetStateAfterSave = () => {
        this.isDirty = false
    }

    @action setStoreItems = (data) => {
        this.listOfPeriods = data
    }

    @action updatePeriodName = (e, index) => {
        this.listOfPeriods[index].periodDescription = e.target.value
        this.isDirty = true
    }
}

export default new PeriodNamesStore()