import React from 'react'
import { inject, observer } from 'mobx-react'
import { TextField } from 'react-md'

@inject('PeriodNamesStore')
@observer
export default class PeriodNamesItem extends React.Component {    
    render() {
        const { periodDescription, periodId } = this.props.details
        const { saveData, updatePeriodName} = this.props.PeriodNamesStore
        return (
            <TextField 
                className="md-cell"
                id={'period-' + periodId}
                onBlur={() => saveData()}
                onChange={(value, e) => updatePeriodName(e, this.props.index)}
                value={periodDescription} />
        )
    }
}