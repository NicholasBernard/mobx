import React from 'react'
import PeriodNamesItem from './PeriodNamesItem'

const PeriodNamesList = (props) => {
    if(props.listOfPeriods === undefined) {
        return <p>Loading Period Names...</p>
    }

    return (
        <div>
            {                             
            props.listOfPeriods.map((period, index) => 
                <PeriodNamesItem
                    details={period}
                    key={props.listOfPeriods[index].periodId}
                    index={index} />)
            }
        </div>
       
    )
}
export default PeriodNamesList