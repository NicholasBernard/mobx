import React from 'react'
import { shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import PeriodNamesItem from '../PeriodNamesItem'

describe('PeriodNamesItem', () => {
    const store = {
        saveData: jest.fn(),
        updatePeriodName: jest.fn()
    }

    const details = {
        periodDescription: 'Test Period 1',
        periodId: 1,
        schoolId: 1,
        schoolPeriodId: 0
    }

    const wrapper = shallow(
        <PeriodNamesItem.wrappedComponent
            details={details}
            index={0}
            PeriodNamesStore={store} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })

    it('should call savePeriodNames onBlur of textfield', () => {
        wrapper.find('#period-1').simulate('blur')
        expect(store.saveData).toHaveBeenCalledTimes(1)
    })

    it('should call updatePeriodName onChange of textfield', () => {
        const event = { target: { value: 'Testing'}}

        wrapper.find('#period-1').simulate('change', event, 0)
        expect(store.updatePeriodName).toHaveBeenCalledTimes(1)
    })
})