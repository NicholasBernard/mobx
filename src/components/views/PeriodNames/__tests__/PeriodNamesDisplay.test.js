import React from 'react'
import { shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import PeriodNamesDisplay from '../PeriodNamesDisplay'

describe('PeriodNamesDisplay', () => {
    const store = {
        fetchData: jest.fn()
    }

    const wrapper = shallow(
        <PeriodNamesDisplay.wrappedComponent
            PeriodNamesStore={store} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.fetchData).toHaveBeenCalledTimes(1)
    })
})