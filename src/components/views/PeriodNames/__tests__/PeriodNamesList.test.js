import React from 'react'
import { shallow } from 'enzyme'
import PeriodNamesList from '../PeriodNamesList'

describe('PeriodNamesList', () => {
    const listOfPeriods = [
        {
            periodDescription : "Period 1",
            periodId: 1,
            schoolId: 1,
            schoolPeriodId: 0  
        },
        {
            periodDescription : "Period 2",
            periodId: 2,
            schoolId: 1,
            schoolPeriodId: 0  
        }
    ]

    it('should render a loading message is listOfPeriods is undefined', () => {
        const wrapperUndefined = shallow(
            <PeriodNamesList />
        )
        expect(wrapperUndefined).toMatchSnapshot()
        expect(wrapperUndefined.find('p').text()).toBe('Loading Period Names...')
    })

    it('should render correctly when listOfPeriods is defined', () => {
        const wrapper = shallow(
            <PeriodNamesList 
                listOfPeriods={listOfPeriods} />
        )
        expect(wrapper.children().length).toBe(2)
    })
})