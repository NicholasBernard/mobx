import React from 'react'
import { PeriodNamesStore } from '../PeriodNamesStore'

describe('PeriodNamesStore', () => {
    const store = new PeriodNamesStore()

    it('resets data correctly on resetStateAfterSave action', () => {
        store.isDirty = true

        store.resetStateAfterSave()

        expect(store.isDirty).toBe(false)
    })

    it('sets observable correctly on setStoreItems action', () => {
        const data = {
            0:{
                periodDescription : "Period 1",
                periodId: 1,
                schoolId: 1,
                schoolPeriodId: 0  
            },
            1:{
                periodDescription : "Period 2",
                periodId: 2,
                schoolId: 1,
                schoolPeriodId: 0  
            },
        }
            
        store.setStoreItems(data)
        expect(store.listOfPeriods).toEqual(data)
    })

    it('updates correct period name by index on updatePeriodName action', () => {
        const listOfPeriods = {
            0:{
                periodDescription : "Period 1",
                periodId: 1,
                schoolId: 1,
                schoolPeriodId: 0  
            },
            1:{
                periodDescription : "Period 2",
                periodId: 2,
                schoolId: 1,
                schoolPeriodId: 0  
            },
        }
        const event = { target: { value: 'Updated Period Name'}}
        store.listOfPeriods = listOfPeriods

        store.updatePeriodName(event, 0)
        expect(store.listOfPeriods[0].periodDescription).toBe('Updated Period Name')
    })
})