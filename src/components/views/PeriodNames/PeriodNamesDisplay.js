import React from 'react'
import { inject, observer } from 'mobx-react'
import PageHeader from '../../common/PageHeader'
import NavButtons from '../../common/buttons/NavButtons'
import PeriodNamesList from './PeriodNamesList'

@inject('PeriodNamesStore')
@observer
export default class PeriodNamesDisplay extends React.Component {
    componentWillMount() {
        this.props.PeriodNamesStore.fetchData()
    }

    render() {
        return (
            <div>
                <PageHeader headerText='Period Names' />
                <p>Now we are going to assign names for each period you just declared.</p>
                <PeriodNamesList
                    listOfPeriods={this.props.PeriodNamesStore.listOfPeriods} />
                <NavButtons 
                    prevPage="/periodsPrefix"
                    nextPage="/prebookSettings" />
            </div>
        )
    }
}