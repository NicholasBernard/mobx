import React from 'react'
import { NavButton } from '../../common/buttons/NavButton'

const WelcomeDisplay = () => (
    <div>
        <div>
            <h1>Welcome to Enriching Students!</h1>
            <p>The following sections will help you import your schools data and setup your account.</p>
            <p>Each of the pages can be navigated by using the Next button of by using the menu of the left.  Your data will save as you navigate through the site.</p>
            <div id='nav-buttons-container'>
                <NavButton
                    id='next-button'
                    label='Next'
                    link='/periodsPrefix'
                    primary={true} />
            </div>
        </div>
    </div>
)

export default WelcomeDisplay