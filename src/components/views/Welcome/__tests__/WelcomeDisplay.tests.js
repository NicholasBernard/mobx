import React from 'react'
import { shallow } from 'enzyme'
import WelcomeDisplay from '../WelcomeDisplay'

describe('WelcomeDisplay', () => {
    const wrapper = shallow(
        <WelcomeDisplay />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })
});