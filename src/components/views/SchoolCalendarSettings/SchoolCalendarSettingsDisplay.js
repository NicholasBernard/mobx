import React from 'react'
import { inject, observer } from 'mobx-react'
import PageHeader from '../../common/PageHeader'
import SubHeading from '../../common/SubHeading'
import NavButtons from '../../common/buttons/NavButtons'
import BlockDateForm from './BlockDateForm'
import BlockedDatesList from './BlockedDatesList'

@inject('SchoolCalendarSettingsStore', 'PeriodNamesStore')
@observer
export default class SchoolCalendarSettingsDisplay extends React.Component{
    componentWillMount() {
        this.props.SchoolCalendarSettingsStore.fetchData()
        this.props.PeriodNamesStore.fetchData()
    }

    render(){
        return(
            <div>
                <PageHeader headerText="School Calendar Settings" />
                <p>Now we can start blocking off days of the year that you don't want to schedule students.  Examples might include holidays, school events or days with a teacher workshop.  If you don't have access to all the dates that need to be blocked, a staff member with Admin privileges can continue to block dates from within Enriching Students.</p>
                <BlockDateForm 
                    listOfPeriods={this.props.PeriodNamesStore.listOfPeriods}
                    />
                <SubHeading text="Blocked Dates" />
                <BlockedDatesList
                    listOfBlockedDates={this.props.SchoolCalendarSettingsStore.listOfBlockedDates} />
                <NavButtons
                    prevPage="/appointmentTypeSettings"
                    nextPage="/blockDateSettings" />
            </div>
        )
    }
}