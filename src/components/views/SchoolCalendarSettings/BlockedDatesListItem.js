import React from 'react'
import { inject, observer} from 'mobx-react'
import { Button, SelectionControl } from 'react-md'
import ConfirmDeleteButtons from '../../common/buttons/ConfirmDeleteButtons'
import BlockedDateDetailsList from './BlockedDateDetailsList'

@inject('SchoolCalendarSettingsStore')
@observer
export default class BlockedDateItem extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            showConfirmDeleteOptions: false,
            showDetails: false
        }
    }

    handleConfirmRemove = () => {
        this.setState({
            showConfirmDeleteOptions: false
        }, this.props.SchoolCalendarSettingsStore.deleteBlockedDate(this.props.details.courseToDateBlockedId, this.props.index))
    }

    toggleButtonVisibililty = () => {
        this.setState({
            showConfirmDeleteOptions: !this.state.showConfirmDeleteOptions
        })
    }

    toggleDetailsVisibility = (checked) => {
        this.setState({
            showDetails: checked
        })
    }

    render(){
        const {blockedDetails, blockedReason, courseToDateBlockedId, showDetails} = this.props.details
        return(
            <div>
                <div className="blocked-dates-list">
                    <div>
                        {blockedReason}
                    </div>
                    <div>
                        <SelectionControl
                            checked={showDetails}
                            id={"show-details-for-" + courseToDateBlockedId}
                            label="Show Details"
                            labelBefore={true}
                            name={"show-details-for" + blockedReason}
                            onChange={(checked => this.toggleDetailsVisibility(checked))}
                            type="checkbox" />
                    </div>
                    <div>
                        {
                            this.state.showConfirmDeleteOptions ?
                                <ConfirmDeleteButtons
                                    id={courseToDateBlockedId}
                                    onClickCancel={() => this.toggleButtonVisibililty()}
                                    onClickConfirm={() => this.handleConfirmRemove()} />
                            : 
                                <Button
                                    className='delete-button'
                                    id={'delete-block-date-' + courseToDateBlockedId}
                                    onClick={() => this.toggleButtonVisibililty()}
                                    raised>
                                        Remove
                                </Button>                        
                        }                    
                    </div>
                </div>            
                {                    
                    this.state.showDetails
                        ?
                            <BlockedDateDetailsList 
                                blockedDetails={blockedDetails}
                                index={this.props.index} />    
                        : null
                }
            </div>
        )}
}