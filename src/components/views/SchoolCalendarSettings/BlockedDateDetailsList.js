import React from 'react'
import BlockedDateDetailsListItem from './BlockedDateDetailsListItem'

const BlockedDateDetailsList = (props) => (
    <div>
        {
            props.blockedDetails.map((detail, index) =>
                <BlockedDateDetailsListItem 
                    details={detail}
                    key={index}
                    index={index}
                    listIndex={props.index} />
            )
        }
    </div>
)

export default BlockedDateDetailsList