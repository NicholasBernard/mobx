import axios from 'axios'
import { action, observable } from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class SchoolCalendarSettingsStore {
    @observable listOfBlockedDates = []

    createBlockedDate = (blockedDate) => {
        /* istanbul ignore next */
        if(blockedDate.blockedReason === ''){
            return
        }
        /* istanbul ignore next */
        axios.post(`${ROOT_URL}schoolcalendar/add/`, blockedDate, createESAuthToken())
        .then(response => {this.fetchData()})
    }

    deleteBlockedDate = (id, i) => {
        /* istanbul ignore next */
        axios.delete(`${ROOT_URL}schoolcalendar/delete/${id}`, createESAuthToken())
        .then(response => {this.removeDateFromArray(i)})
    }

    deleteBlockedDateDetail = (id, index, listIndex) => {
        /* istanbul ignore next */
        axios.delete(`${ROOT_URL}schoolcalendar/deletedetail/${id}`, createESAuthToken())
        .then(response => this.removeDetailFromArray(index, listIndex))
    }

    fetchData() {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}schoolcalendar/get/`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    @action removeDateFromArray = (i) => {
        const newArray = [
            ...this.listOfBlockedDates.slice(0, i),
            ...this.listOfBlockedDates.slice(i + 1)
        ]

        this.listOfBlockedDates = newArray
    }

    @action removeDetailFromArray = (index, listIndex) => {
        const details = [
            ...this.listOfBlockedDates[listIndex].blockedDetails.slice(0, index),
            ...this.listOfBlockedDates[listIndex].blockedDetails.slice(index + 1),
        ]

        const listOfBlockedDates = this.listOfBlockedDates.slice()
        listOfBlockedDates[listIndex].blockedDetails = details

        this.listOfBlockedDates = listOfBlockedDates
    }

    @action setStoreItems = (data) => {
        this.listOfBlockedDates = data
    }
}

export default new SchoolCalendarSettingsStore()