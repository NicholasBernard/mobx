import React from 'react'
import { Button } from 'react-md'
import { inject, observer } from 'mobx-react'
import Moment from 'react-moment'
import ConfirmDeleteButtons from '../../common/buttons/ConfirmDeleteButtons'

@inject('SchoolCalendarSettingsStore')
@observer
export default class BlockedDateDetailsListItem extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            showConfirmDeleteOptions: false,
        }
    }

    handleConfirmRemove = () => {
        this.setState({
            showConfirmDeleteOptions: false
        }, this.props.SchoolCalendarSettingsStore.deleteBlockedDateDetail(this.props.details.courseToDateBlockedDetailId, this.props.index, this.props.listIndex))
    }

    toggleButtonVisibililty = () => {
        this.setState({
            showConfirmDeleteOptions: !this.state.showConfirmDeleteOptions
        })
    }

    render(){
        return(
            <div className="blocked-dates-details"> 
                <div>
                    {this.props.details.periodDescription} - <Moment format='YYYY/MM/DD'>{this.props.details.scheduleDate}</Moment>
                </div>
                <div>
                    {
                        this.state.showConfirmDeleteOptions ?
                            <ConfirmDeleteButtons
                                id={this.props.details.courseToDateBlockedDetailId}
                                onClickCancel={() => this.toggleButtonVisibililty()}
                                onClickConfirm={() => this.handleConfirmRemove()} />
                        :
                            <Button
                                className='delete-button'
                                id={'remove-detail-' + this.props.details.courseToDateBlockedDetailId}
                                onClick={() => this.toggleButtonVisibililty()}
                                raised>
                                Remove
                            </Button>
                    }
                </div>
            </div>
        )
    }
}