import React from 'react'
import { inject, observer } from 'mobx-react'
import { Button, DatePicker, SelectionControl, TextField } from 'react-md'

@inject('SchoolCalendarSettingsStore')
@observer
export default class BlockDateForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            endDate: new Date(),
            listOfWeekdays: [
                {
                    id: 1,
                    description: 'Monday',
                    isSelected: false
                },
                {
                    id: 2,
                    description: 'Tuesday',
                    isSelected: false
                },
                {
                    id: 3,
                    description: 'Wednesday',
                    isSelected: false
                },
                {
                    id: 4,
                    description: 'Thursday',
                    isSelected: false
                },
                {
                    id: 5,
                    description: 'Friday',
                    isSelected: false
                },
            ],
            listOfPeriods: props.listOfPeriods ? props.listOfPeriods : [],
            periodsAllChecked: false,
            periodsToInclude: [],
            reason: '',
            showPeriodErrorMessage: false,
            showWeekdayErrorMessage: false,
            startDate: new Date(),
            weekdaysAllChecked: false,
            weekdaysToInclude: [],            
        }
    }

    validateWeekdayHasBeenSelected = (array) => {
        // if the array length is 0, no weekday has been selected
        // we should display an error 
        if(array.length === 0){
            this.setState({
                showWeekdayErrorMessage: true
            })
            return
        }else{
            // otherwise we know a weekday has been selected, no errors are necessary
            this.setState({
                showWeekdayErrorMessage: false
            })
        }        
    }

    validatePeriodHasBeenSelected = (array) => {
        // if the array length is 0, no period has been selected
        // we should display an error 
        if(array.length === 0){
            this.setState({
                showPeriodErrorMessage: true
            })
            return
        }else{
            // otherwise we know a period has been selected, no errors are necessary
            this.setState({
                showPeriodErrorMessage: false
            })
        }        
    }



    componentWillReceiveProps = (nextProps) => {
        /* istanbul ignore next */
        let listOfPeriods = nextProps.listOfPeriods.map((period) => {
            period.isSelected = false;
            return period;
        })
        /* istanbul ignore next */
        this.setState({
            listOfPeriods: listOfPeriods
        })
    }

    checkAllPeriods = (checked) => {
        let listOfPeriods = []
        for(let i = 0; i < this.state.listOfPeriods.length; i++){
            const period = this.state.listOfPeriods[i]         
            const updatedPeriod = {
                ...period,
                isSelected: checked
            }
            listOfPeriods.push(updatedPeriod);                        
        }
        this.setState({            
            listOfPeriods: listOfPeriods,
            periodsAllChecked: checked
        })
    }

    checkAllWeekdays = (checked) => {
        let listOfWeekdays = []
        for(let i = 0; i < this.state.listOfWeekdays.length; i++){
            const weekday = this.state.listOfWeekdays[i]     
            const updatedWeekday = {
                ...weekday,
                isSelected: checked
            }
            listOfWeekdays.push(updatedWeekday)                
        }
        this.setState({
            listOfWeekdays: listOfWeekdays,
            weekdaysAllChecked: checked
        })
    }

    handleSubmit = (event) => {
        event.preventDefault()

        let periodsToInclude = []
        for(let i = 0; i < this.state.listOfPeriods.length; i++){
            if(this.state.listOfPeriods[i].isSelected){
                periodsToInclude.push(i+1);
            }
        }

        let weekdaysToInclude = []
        for(let i = 0; i < this.state.listOfWeekdays.length; i++){
            if(this.state.listOfWeekdays[i].isSelected){
                weekdaysToInclude.push(i+1);
            }
        }

        this.validateWeekdayHasBeenSelected(weekdaysToInclude)
        this.validatePeriodHasBeenSelected(periodsToInclude)

        if(this.state.reason === '' || weekdaysToInclude.length === 0 || periodsToInclude.length === 0){
            return
        }

        const blockedDate = {
            blockedReason: this.state.reason,
            startDate: this.state.startDate,
            endDate: this.state.endDate,
            periodsToInclude: periodsToInclude,
            weekdaysToInclude: weekdaysToInclude,
            schoolId: 0
        }

        this.setState({
            endDate: new Date(),
            periodsAllChecked: false,
            reason: '',
            startDate: new Date(),
            weekdaysAllChecked: false
        }, this.props.SchoolCalendarSettingsStore.createBlockedDate(blockedDate))
        
        this.checkAllWeekdays(false)
        this.checkAllPeriods(false)  
    }

    onEndDateChange = (date) => {
        const updatedDate = new Date(date)
        this.setState({
            endDate: updatedDate
        })
    }

    onStartDateChange = (date) => {
        const updatedDate = new Date(date)
        
        //get day from updateDate and set weekday checkbox
        const weekday = updatedDate.getDay()
        this.weekdayOnChange(true, weekday - 1)

        if(updatedDate <= this.state.endDate){
            this.setState({
                startDate: updatedDate
            })
        }else{
            this.setState({
                endDate: updatedDate,
                startDate: updatedDate
            })
        }
    }

    onReasonChange = (value) => {
        this.setState({
            reason: value
        })
    }

    periodOnChange = (checked, index) => {
        const period = this.state.listOfPeriods[index]
        const updatedPeriod = {
            ...period,
            isSelected: checked
        };
        const listOfPeriods = this.state.listOfPeriods
        listOfPeriods[index] = updatedPeriod
        this.setState({listOfPeriods})     
    }

    weekdayOnChange = (checked, index) => {
        const weekday = this.state.listOfWeekdays[index]
        const updatedWeekday = {
            ...weekday,
            isSelected: checked
        }
        const listOfWeekdays = this.state.listOfWeekdays
        listOfWeekdays[index] = updatedWeekday
        this.setState({listOfWeekdays})   
    }

    render(){
        const { endDate, periodsAllChecked, startDate, weekdaysAllChecked } = this.state
        return(
            <form onSubmit={(e) => this.handleSubmit(e)} >     
                <div>
                    <p>First you must provide a name for the blocked date.  Ex. Graduation</p>
                    <TextField
                        className="md-cell md-cell--bottom"
                        errorText="Please provide a reason for blocked date"
                        id="reason-for-block"
                        label="Reason for Block"
                        onChange={(value, e) => this.onReasonChange(value)}
                        required
                        value={this.state.reason} />
                </div>
                <div>
                    <p>Next we need to determine a date range for this block.</p>
                    <DatePicker
                        autoOk={true}
                        className="md-cell"
                        disableWeekEnds={true}
                        errorText="Please select a start date"
                        id="start-date"
                        label="Start Date"
                        onChange={(dateString) => this.onStartDateChange(dateString)}
                        required
                        value={startDate}
                        />
                    <DatePicker
                        autoOk={true}
                        className="md-cell"
                        disableWeekEnds={true}
                        errorText="Please select an end date"
                        id="end-date"
                        label="End Date"
                        minDate={endDate}
                        onChange={(dateString) => this.onEndDateChange(dateString)}
                        required
                        value={endDate}
                        />
                </div>
                <div className="listOfPeriods">
                    <SelectionControl
                        checked={weekdaysAllChecked}
                        id='weekday-all'
                        label="All"
                        name="all"
                        onChange={(checked) => this.checkAllWeekdays(checked)}
                        type="checkbox" />
                {
                    this.state.listOfWeekdays.map((day, index) => 
                        <SelectionControl
                            checked={day.isSelected}
                            id={day.description + '-' + day.id}
                            key={index}
                            label={day.description}
                            name="weekday"
                            onChange={(checked) => this.weekdayOnChange(checked, index)}
                            type="checkbox" />)
                }
                {
                    this.state.showWeekdayErrorMessage &&
                    <p className='error-text'>Please select at least one weekday to block</p>
                }                    
                </div>
                <div className="listOfPeriods">
                    <SelectionControl
                        checked={periodsAllChecked}
                        id="periods-all"
                        label="All"
                        name="All"
                        onChange={(checked) => this.checkAllPeriods(checked)} 
                        type="checkbox" />
                    {   
                        this.state.listOfPeriods ?

                        this.state.listOfPeriods.map((period, index) => 
                            <SelectionControl
                                checked={period.isSelected}
                                id={'period-' + period.periodId}
                                key={index}
                                label={period.periodDescription}
                                name="period"
                                onChange={(checked) => this.periodOnChange(checked, index)}
                                type="checkbox"/>)
                        
                                : null
                    }
                    {
                        this.state.showPeriodErrorMessage &&
                        <p className='error-text'>Please select at least one period to block</p>
                    }       
                </div>
                <div>
                    <Button                         
                        className="md-cell"
                        primary
                        raised
                        type="submit">Block
                    </Button>
                </div>
            </form>
        )
    }
}