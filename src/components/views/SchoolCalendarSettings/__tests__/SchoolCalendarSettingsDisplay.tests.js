import { shallow } from 'enzyme'
import React from 'react'
import { wrappedComponent } from 'mobx'
import { listOfBlockedDates } from '../../../../testData/schoolCalendarSettings'
import SchoolCalendarSettingsDisplay from '../SchoolCalendarSettingsDisplay'

describe('SchoolCalendarSettingsDisplay', () => {
    const store = {
        fetchData: jest.fn(),
        listOfBlockedDates: []
    }

    const periodNamesStore = {
        fetchData: jest.fn()
    }

    const wrapper = shallow(
        <SchoolCalendarSettingsDisplay.wrappedComponent
            SchoolCalendarSettingsStore={store}
            PeriodNamesStore={periodNamesStore} />
    )

    it('renders correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.fetchData).toHaveBeenCalledTimes(1)
        expect(periodNamesStore.fetchData).toHaveBeenCalledTimes(1)
    })
})