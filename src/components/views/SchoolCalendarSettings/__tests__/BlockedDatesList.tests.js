import React from 'react'
import { shallow } from 'enzyme'
import { listOfBlockedDates } from '../../../../testData/schoolCalendarSettings'
import BlockedDatesList from '../BlockedDatesList'
import BlockedDatesListItem from '../BlockedDatesListItem'

describe('BlockedDatesList', () => {
    const wrapper = shallow(
        <BlockedDatesList
            listOfBlockedDates={listOfBlockedDates} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(wrapper.children().length).toBe(listOfBlockedDates.length)
        for(let i = 0; i < listOfBlockedDates.length; i++) {
            expect(wrapper.childAt(i).type()).toBe(BlockedDatesListItem)
        }
    })
})