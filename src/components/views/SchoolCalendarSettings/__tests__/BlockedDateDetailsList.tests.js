import React from 'react'
import { shallow } from 'enzyme'
import { blockedDetails } from '../../../../testData/schoolCalendarSettings'
import BlockedDatesDetailsList from '../BlockedDateDetailsList'
import BlockedDateDetailsListItem from '../BlockedDateDetailsListItem'

describe('BlockedDatesList', () => {
    const wrapper = shallow(
        <BlockedDatesDetailsList
            blockedDetails={blockedDetails}
            index={0} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(wrapper.children().length).toBe(blockedDetails.length)
        for(let i = 0; i < blockedDetails.length; i++){
            expect(wrapper.childAt(i).type()).toBe(BlockedDateDetailsListItem)
        }
    })
})