import React from 'react'
import { shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import BlockedDateDetailsListItem from '../BlockedDateDetailsListItem'
import { blockedDetail } from '../../../../testData/schoolCalendarSettings'

describe('BlockedDateDetailsListItem', () => {
    const store = {
        deleteBlockedDateDetail: jest.fn()
    }

    const wrapper = shallow(
        <BlockedDateDetailsListItem.wrappedComponent
            details={blockedDetail}
            SchoolCalendarSettingsStore={store} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })

    it('sets the state correctly when toggleButtonVisibililty function is called', () => {
        wrapper.setState({
            showConfirmDeleteOptions: false
        })    

        wrapper.instance().toggleButtonVisibililty()
        expect(wrapper.instance().state.showConfirmDeleteOptions).toBe(true)

        wrapper.instance().toggleButtonVisibililty()
        expect(wrapper.instance().state.showConfirmDeleteOptions).toBe(false)  
    })

    it('sets the state correctly when handleConfirmRemove function is called', () => {
        wrapper.instance().handleConfirmRemove()
        expect(wrapper.instance().state.showConfirmDeleteOptions).toBe(false)
        expect(store.deleteBlockedDateDetail).toHaveBeenCalledTimes(1)
    })

    it('calls toggleButtonVisibililty function onClick of remove button', () => {
        const spy = jest.spyOn(wrapper.instance(), 'toggleButtonVisibililty')
        wrapper.find('#remove-detail-1001').simulate('click')

        expect(spy).toHaveBeenCalledTimes(1)
    })
})