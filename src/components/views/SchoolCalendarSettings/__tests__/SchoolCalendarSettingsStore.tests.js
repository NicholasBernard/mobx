import React from 'react'
import { SchoolCalendarSettingsStore } from '../SchoolCalendarSettingsStore'
import { listOfBlockedDates, listOfBlockedDatesDateDeleted, listOfBlockedDatesDetailDeleted } from '../../../../testData/schoolCalendarSettings'

describe('SchoolCalendarSettingsStore', () => {
    const store = new SchoolCalendarSettingsStore()
    
    it('updates observables correctly on removeDateFromArray action', () => {
        store.listOfBlockedDates = listOfBlockedDates
        store.removeDateFromArray(0)
        expect(store.listOfBlockedDates).toEqual(listOfBlockedDatesDateDeleted)
    })

    it('updates observables correctly on removeDetailFromArray action', () => {
        store.listOfBlockedDates = listOfBlockedDates
        store.removeDetailFromArray(0, 0)
        expect(store.listOfBlockedDates).toEqual(listOfBlockedDatesDetailDeleted)
    })

    it('updates observables correctly on setStoreItems action', () => {
        store.setStoreItems(listOfBlockedDates)
        expect(store.listOfBlockedDates).toEqual(listOfBlockedDates)
    })
})