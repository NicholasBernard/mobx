import React from 'react'
import { shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import { blockedDetail } from '../../../../testData/schoolCalendarSettings'
import BlockedDatesListItem from '../BlockedDatesListItem'

describe('BlockedDatesListItem', () => {
    const store = {
        deleteBlockedDate: jest.fn()
    }

    const wrapper = shallow(
        <BlockedDatesListItem.wrappedComponent
            details={blockedDetail}
            SchoolCalendarSettingsStore={store} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })

    it('should set the state correctly on the toggleButtonVisibililty function', () => {
        wrapper.setState({
            showConfirmDeleteOptions: true
        })

        wrapper.instance().toggleButtonVisibililty()
        expect(wrapper.instance().state.showConfirmDeleteOptions).toBe(false)
        wrapper.instance().toggleButtonVisibililty()
        expect(wrapper.instance().state.showConfirmDeleteOptions).toBe(true)
    })

    it('should set the state correctly on the handleConfirmRemove function', () => {
        wrapper.instance().handleConfirmRemove()
        expect(wrapper.instance().state.showConfirmDeleteOptions).toBe(false)
        expect(store.deleteBlockedDate).toHaveBeenCalledTimes(1)
    })

    it('should set the state correctly on the toggleDetailsVisibility function', () => {
        wrapper.instance().toggleDetailsVisibility(true)
        expect(wrapper.instance().state.showDetails).toBe(true)
        
        wrapper.instance().toggleDetailsVisibility(false)
        expect(wrapper.instance().state.showDetails).toBe(false)
    })

    it('calls toggleDetailsVisibility function onChange of selection control', () => {
        const spy = jest.spyOn(wrapper.instance(), 'toggleDetailsVisibility')
        wrapper.find('#show-details-for-' + blockedDetail.courseToDateBlockedId).simulate('change')
        expect(spy).toHaveBeenCalledTimes(1)
    })

    it('calls toggleButtonVisibililty function onClick of delete button', () => {
        wrapper.setState({
            showConfirmDeleteOptions: false
        })

        const spy = jest.spyOn(wrapper.instance(), 'toggleButtonVisibililty')
        wrapper.find('#delete-block-date-' + blockedDetail.courseToDateBlockedId).simulate('click')
        expect(spy).toHaveBeenCalledTimes(1)
    })
})