import React from 'react'
import BlockedDateForm from '../BlockDateForm'
import { shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import { blockedDateFormDefaultState } from '../../../../testData/schoolCalendarSettings'

describe('BlockedDateForm', () => {
    const store = {
        createBlockedDate: jest.fn()
    }

    const wrapper = shallow(
        <BlockedDateForm.wrappedComponent
            SchoolCalendarSettingsStore={store}/>
    )

    wrapper.instance().state = blockedDateFormDefaultState

    it('should set the state correctly when the checkAllPeriods function is called', () => {
        wrapper.instance().checkAllPeriods(true)
        for(let i = 0; i < wrapper.instance().state.listOfPeriods.length; i++){
            expect(wrapper.instance().state.listOfPeriods[i].isSelected).toBe(true);
        }
        expect(wrapper.instance().state.periodsAllChecked).toBe(true)

        wrapper.instance().checkAllPeriods(false)
        for(let i = 0; i < wrapper.instance().state.listOfPeriods.length; i++){
            expect(wrapper.instance().state.listOfPeriods[i].isSelected).toBe(false)
        }
        expect(wrapper.instance().state.periodsAllChecked).toBe(false)
    })

    it('should set the state correctly when the checkAllWeekdays function is called', () => {
        wrapper.instance().checkAllWeekdays(true)
        for(let i = 0; i < wrapper.instance().state.listOfWeekdays.length; i++){
            expect(wrapper.instance().state.listOfWeekdays[i].isSelected).toBe(true)
        }
        expect(wrapper.instance().state.weekdaysAllChecked).toBe(true)

        wrapper.instance().checkAllWeekdays(false)
        for(let i = 0; i < wrapper.instance().state.listOfWeekdays.length; i++){
            expect(wrapper.instance().state.listOfWeekdays[i].isSelected).toBe(false)
        }
        expect(wrapper.instance().state.weekdaysAllChecked).toBe(false)
    })

    it('should set the state correctly when the onEndDateChange function is called', () => {
        const date = '2/16/2018'
        wrapper.instance().onEndDateChange(date)

        expect(wrapper.instance().state.endDate).toEqual(new Date(date))
    })

    it('should set the state correctly when the onStartDateChange function is called, and the start date is less then the end date', () => {
        const startDate = '2/13/2018'
        const endDate = '2/16/2018'
        wrapper.instance().state.endDate = new Date(endDate)
        wrapper.instance().onStartDateChange(startDate)

        expect(wrapper.instance().state.startDate).toEqual(new Date(startDate))
        expect(wrapper.instance().state.endDate).toEqual(new Date(endDate))
    })

    it('should set the state correctly when the onStartDateChange function is called, and the start date is greater then the end date', () => {
        const startDate = '2/17/2018'
        const endDate = '2/16/2018'
        wrapper.instance().state.endDate = new Date(endDate)
        wrapper.instance().onStartDateChange(startDate)

        expect(wrapper.instance().state.startDate).toEqual(new Date(startDate))
        expect(wrapper.instance().state.endDate).toEqual(new Date(startDate))
    })

    it('should set the state correctly when the onReasonChange function is called', () => {
        wrapper.setState({
            reason: ''
        })
        wrapper.instance().onReasonChange('Testing')
        expect(wrapper.instance().state.reason).toBe('Testing')
    })

    it('should set the state correctly when the periodsOnChange function is called', () => {
        wrapper.instance().periodOnChange(true, 0)
        expect(wrapper.instance().state.listOfPeriods[0].isSelected).toBe(true)
        wrapper.instance().periodOnChange(false, 0)
        expect(wrapper.instance().state.listOfPeriods[0].isSelected).toBe(false)

        wrapper.instance().periodOnChange(true, 3)
        expect(wrapper.instance().state.listOfPeriods[3].isSelected).toBe(true)
        wrapper.instance().periodOnChange(false, 3)
        expect(wrapper.instance().state.listOfPeriods[3].isSelected).toBe(false)
    })

    it('should set the state correctly when the weekdayOnChange function is called', () => {
        wrapper.instance().weekdayOnChange(true, 0)
        expect(wrapper.instance().state.listOfWeekdays[0].isSelected).toBe(true)
        wrapper.instance().weekdayOnChange(false, 0)
        expect(wrapper.instance().state.listOfWeekdays[0].isSelected).toBe(false)

        wrapper.instance().weekdayOnChange(true, 3)
        expect(wrapper.instance().state.listOfWeekdays[3].isSelected).toBe(true)
        wrapper.instance().weekdayOnChange(false, 3)
        expect(wrapper.instance().state.listOfWeekdays[3].isSelected).toBe(false)
    })

    it('should call the handleSubmit function on form submit', () => {
        const spy = jest.spyOn(wrapper.instance(), 'handleSubmit')
        const event = { preventDefault: () => {}}

        wrapper.find('form').simulate('submit', event)
        expect(spy).toHaveBeenCalledTimes(1)
    })

    it('should call the onReasonChange function onChange of #reason-for-block', () => {
        const spy = jest.spyOn(wrapper.instance(), 'onReasonChange')
        wrapper.find('#reason-for-block').simulate('change', 'Testing')
        expect(spy).toHaveBeenCalledTimes(1)
        expect(spy).toHaveBeenCalledWith('Testing')
    })

    it('should call the onStartDateChange function onChange of #start-date', () => {
        const spy = jest.spyOn(wrapper.instance(), 'onStartDateChange')
        wrapper.find('#start-date').simulate('change', '2018-10-11')
        expect(spy).toHaveBeenCalledTimes(1)
    })

    it('should call the onEndDateChange function onChange of #end-date', () => {
        const spy = jest.spyOn(wrapper.instance(), 'onEndDateChange')
        wrapper.find('#end-date').simulate('change', '2018-10-11')
        expect(spy).toHaveBeenCalledTimes(1)
    })

    it('should call the checkAllWeekdays function from #weekday-all', () => {
        wrapper.setState({
            blockedDateFormDefaultState
        })
        const spy = jest.spyOn(wrapper.instance(), 'checkAllWeekdays')
        wrapper.find('#weekday-all').simulate('change')
        expect(spy).toHaveBeenCalledTimes(1)
    })

    it('should call the weekdayOnChange function from #Monday-1', () => {
        wrapper.setState({
            blockedDateFormDefaultState
        })
        const spy = jest.spyOn(wrapper.instance(), 'weekdayOnChange')
        wrapper.find('#Monday-1').simulate('change')
        expect(spy).toHaveBeenCalledTimes(1)
    })

    it('should call the checkAllPeriods function from #periods-all', () => {
        wrapper.setState({
            blockedDateFormDefaultState
        })
        const spy = jest.spyOn(wrapper.instance(), 'checkAllPeriods')
        wrapper.find('#periods-all').simulate('change')
        expect(spy).toHaveBeenCalledTimes(1)
    })

    it('should call the periodOnChange function from #period-1', () => {
        wrapper.setState({
            blockedDateFormDefaultState
        })
        const spy = jest.spyOn(wrapper.instance(), 'periodOnChange')
        wrapper.find('#period-1').simulate('change')
        expect(spy).toHaveBeenCalledTimes(1)
    })
})