import React from 'react'
import BlockedDatesListItem from './BlockedDatesListItem'

const BlockedDatesList = (props) => (
    <div>            
        {
            props.listOfBlockedDates.map((blockedDate, index) => 
                <BlockedDatesListItem
                    details={blockedDate}
                    key={index}
                    index={index} />)
        }
    </div>        
)

export default BlockedDatesList