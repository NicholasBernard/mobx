import React from 'react'
import PrebookSettingsDisplay from '../PrebookSettingsDaysDisplay'
import {shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'

describe('PrebookSettingsDisplay', () => {
    const store = {
        Monday:{
            name: 'Monday',
            checked: false
        },
        Tuesday:{
            name: 'Tuesday',
            checked: false
        },
        Wednesday:{
            name: 'Wednesday',
            checked: false
        },
        Thursday:{
            name: 'Thursday',
            checked: false
        },
        Friday:{
            name: 'Friday',
            checked: false
        },
        fetchData: jest.fn(),
        saveData: jest.fn()
    }

    const wrapper = shallow(
        <PrebookSettingsDisplay.wrappedComponent
            PrebookSettingsDaysStore={store} />
    )

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.fetchData).toHaveBeenCalledTimes(1)
    })

    it('should call savePrebookSettingsDays onChange of SelectionControl', () => {
        const event = { target: { value: 'Big Time Test!'}}

        wrapper.find('#Monday').simulate('change', event)
        expect(store.saveData).toHaveBeenCalledTimes(1)
    })
})