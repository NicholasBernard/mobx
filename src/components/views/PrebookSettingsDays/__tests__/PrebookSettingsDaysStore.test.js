import React from 'react'
import {PrebookSettingsDaysStore} from '../PrebookSettingsDaysStore'

describe('PrebookSettingsDaysStore', () => {
    const store = new PrebookSettingsDaysStore()
    store.Monday = {
        name: 'Monday',
        checked: false
    }
    store.Tuesday = {
        name: 'Tuesday',
        checked: true
    }
    store.Wednesday = {
        name: 'Wednesday',
        checked: false
    }
    store.Thursday = {
        name: 'Thursday',
        checked: true
    }
    store.Friday = {
        name: 'Friday',
        checked: false
    }

    it('should set observable correctly on createSaveObject action', () => {
        store.createObjectToSave()
        expect(store.objectToSave).toEqual({
            prebookMonday: false,
            prebookTuesday: true,
            prebookWednesday: false,
            prebookThursday: true,
            prebookFriday: false
        })
    })

    it('should set observables correctly on resetStateAfterSave action', () => {
        store.isDirty = true
        store.objectToSave = {
            prebookMonday: store.Monday.checked,
            prebookTuesday: store.Tuesday.checked,
            prebookWednesday: store.Wednesday.checked,
            prebookThursday: store.Thursday.checked,
            prebookFriday: store.Friday.checked
        }

        store.resetStateAfterSave()
        expect(store.objectToSave).toEqual({})
    })

    it('should set observables correctly on setStoreItems action', () => {
        const data = {
            prebookMonday: true,
            prebookTuesday: true,
            prebookWednesday: true,
            prebookThursday: false,
            prebookFriday: false
        }
        store.setStoreItems(data)
        expect(store.Monday.checked).toBe(data.prebookMonday)
        expect(store.Tuesday.checked).toBe(data.prebookTuesday)
        expect(store.Wednesday.checked).toBe(data.prebookWednesday)
        expect(store.Thursday.checked).toBe(data.prebookThursday)
        expect(store.Friday.checked).toBe(data.prebookFriday)
    })

    it('should set observable correctly on updateSelectedDay action', () => {
        const eventTuesday = { target: { name: 'Tuesday', checked: false}}
        const eventFriday = { target: { name: 'Friday', checked: true}}

        store.updateSelectedDay(eventTuesday)
        expect(store.Tuesday.checked).toBe(eventTuesday.target.checked)
        expect(store.isDirty).toBe(true)

        store.updateSelectedDay(eventFriday)
        expect(store.Friday.checked).toBe(eventFriday.target.checked)
        expect(store.isDirty).toBe(true)
    })

    it('should do something correctly', () => {
        const event = { target: { name: 'Tuesday', checked: true}}
        const updateSpy = jest.spyOn(store, 'updateSelectedDay')
        const objectSpy = jest.spyOn(store, 'createObjectToSave')

        store.saveData(event)

        expect(updateSpy).toHaveBeenCalledTimes(1)
        expect(updateSpy).toHaveBeenCalledWith(event)
        expect(objectSpy).toHaveBeenCalledTimes(1)
    })
})