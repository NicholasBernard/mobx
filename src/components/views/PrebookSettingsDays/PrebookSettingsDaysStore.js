import axios from 'axios'
import { action, observable } from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class PrebookSettingsDaysStore {
    @observable Monday = {
        name: 'Monday',
        checked: false
    }
    @observable Tuesday = {
        name: 'Tuesday',
        checked: false
    }
    @observable Wednesday = {
        name: 'Wednesday',
        checked: false
    }
    @observable Thursday = {
        name: 'Thursday',
        checked: false
    }
    @observable Friday = {
        name: 'Friday',
        checked: false
    }
    @observable objectToSave = {}

    fetchData() {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}prebooksettingsdays/get/`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    saveData = (e) => {        
        this.updateSelectedDay(e)        
        this.createObjectToSave()
        /* istanbul ignore next */
        axios.put(`${ROOT_URL}prebooksettingsdays/save/`, this.objectToSave, createESAuthToken())    
    }

    @action createObjectToSave = () => {
        const objectToSave = {
            prebookMonday: this.Monday.checked,
            prebookTuesday: this.Tuesday.checked,
            prebookWednesday: this.Wednesday.checked,
            prebookThursday: this.Thursday.checked,
            prebookFriday: this.Friday.checked,
        }
        this.objectToSave = objectToSave
    }

    @action resetStateAfterSave = () => {
        this.objectToSave = {}
    }

    @action setStoreItems = (data) => {
        this.Monday.checked = data.prebookMonday
        this.Tuesday.checked = data.prebookTuesday
        this.Wednesday.checked = data.prebookWednesday
        this.Thursday.checked = data.prebookThursday
        this.Friday.checked = data.prebookFriday
    }

    @action updateSelectedDay = (e) => {
        this[e.target.name].checked = e.target.checked
    }
}

export default new PrebookSettingsDaysStore()