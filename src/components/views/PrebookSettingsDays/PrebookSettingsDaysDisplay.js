import React from 'react'
import { inject, observer } from 'mobx-react'
import { SelectionControl } from 'react-md'
import NavButtons from '../../common/buttons/NavButtons'
import PageHeader from '../../common/PageHeader'

@inject('PrebookSettingsDaysStore')
@observer
export default class PrebookSettingsDaysDisplay extends React.Component {
    componentWillMount() {
        this.props.PrebookSettingsDaysStore.fetchData()
    }

    renderSelectionControl(props) {
        return (
            <SelectionControl
                checked={props.checked}
                id={props.name}
                label={props.name}
                name={props.name}
                onChange={(checked, e) => this.props.PrebookSettingsDaysStore.saveData(e)}
                type="checkbox" />
        )        
    }

    render() {
        const { Monday, Tuesday, Wednesday, Thursday, Friday } = this.props.PrebookSettingsDaysStore
        return(
            <div>                
                <PageHeader headerText="Prebook Settings - Days" />
                <p>Next, we are going to configure your prebook settings.  When data is originally imported, or when a student is added to Enriching Students, we can determine what appointments will be automatically created.</p>
                { this.renderSelectionControl(Monday) }
                { this.renderSelectionControl(Tuesday) }
                { this.renderSelectionControl(Wednesday) }
                { this.renderSelectionControl(Thursday) }
                { this.renderSelectionControl(Friday) }                
                <NavButtons 
                    prevPage="/periodNames"
                    nextPage="/prebookSettingsPeriods" />
            </div>
        )
    }
}