import axios from 'axios'
import { observable, action } from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class PeriodsPrefixStore {
    @observable homeroomPrefix = ''
    @observable numberOfPeriods = ''
    @observable objectToSave = {}
    @observable isDirty = false

    fetchData = () => {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}periodsprefix/get/`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    saveData = () => {
        /* istanbul ignore next */
        if(this.isDirty){
            this.createObjectToSave()            
            axios.put(`${ROOT_URL}periodsprefix/save/`, this.objectToSave, createESAuthToken())
            this.resetStateAfterSave()
        }
    }

    @action createObjectToSave = () => {
        const objectToSave = {
            homeroomPrefix: this.homeroomPrefix,
            numberOfPeriods: this.numberOfPeriods
        }

        this.objectToSave = objectToSave
    }

    @action resetStateAfterSave = () => {
        this.isDirty = false
        this.objectToSave = {}
    }

    @action setStoreItems = (data) => { 
        this.homeroomPrefix = data.homeroomPrefix
        this.numberOfPeriods = data.numberOfPeriods
    }

    @action updateStateProperty = (e) => {
        this[e.target.name] = e.target.value
        this.isDirty = true
    }
}

export default new PeriodsPrefixStore()