import { shallow, mount } from 'enzyme'
import React from 'react'
import { wrappedComponent } from 'mobx'

import PeriodsPrefixDisplay from '../PeriodsPrefixDisplay'

describe('PeriodsPrefixDisplay', () => {
    const testStore = {
        homeroomPrefix: '',
        numberOfPeriods: '',
        fetchData: jest.fn(),        
        saveData: jest.fn(),
        updateStateProperty: jest.fn()
    }

    const wrapper = shallow(        
        <PeriodsPrefixDisplay.wrappedComponent
            PeriodsPrefixStore={ testStore } />        
    )

    it('renders correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(testStore.fetchData).toHaveBeenCalledTimes(1)
    })

    it('calls the savePeriodsPrefix function from each textfield onBlur event', () => {
        wrapper.find('#numberOfPeriods').simulate('blur')
        expect(testStore.saveData).toHaveBeenCalledTimes(1)

        wrapper.find('#homeroomPrefix').simulate('blur')
        expect(testStore.saveData).toHaveBeenCalledTimes(2)
    })

    it('calls the updateStateProperty function from each textfield onChange event', () => {
        const periodsEvent = { target: { name: 'numberOfPeriods', value: '2'}}
        const homeroomEvent = { target: { name: 'homeroomPrefix', value: 'HR Test'}}

        wrapper.find('#numberOfPeriods').simulate('change', periodsEvent)
        expect(testStore.updateStateProperty).toHaveBeenCalledTimes(1)

        wrapper.find('#homeroomPrefix').simulate('change', homeroomEvent)
        expect(testStore.updateStateProperty).toHaveBeenCalledTimes(2)
    })
})