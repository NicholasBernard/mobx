import React from 'react'
import { PeriodsPrefixStore } from '../PeriodsPrefixStore'

describe('PeriodsPrefixStore', () => {
    const store = new PeriodsPrefixStore()

    it('updates saveObject observable correctly on createObjectToSave action', () => {
        store.homeroomPrefix = 'Hr Test'
        store.numberOfPeriods = '4'

        store.createObjectToSave()
        
        expect(store.objectToSave).toEqual({
            homeroomPrefix: 'Hr Test',
            numberOfPeriods: '4'
        })
    })

    it('updates observables correctly on resetStateAfterSave action', () => {
        store.isDirty = true
        store.objectToSave = {
            homeroomPrefix: 'Hr Test',
            numberOfPeriods: '4'
        }

        store.resetStateAfterSave()

        expect(store.isDirty).toBe(false)
        expect(store.objectToSave).toEqual({})        
    })

    it('updates observables correctly on setStoreItems action', () => {
        const testStore = {
            homeroomPrefix: 'Hr Test',
            numberOfPeriods: '4'
        }

        store.setStoreItems(testStore)
        expect(store.homeroomPrefix).toBe('Hr Test')
        expect(store.numberOfPeriods).toBe('4')
    })

    it('updates observables correctly on updateStateProperty action', () => {
        const homeroomEvent = { target: { name: 'homeroomPrefix', value: 'Big Time Test' }}
        const periodsEvent = { target: { name: 'numberOfPeriods', value: '10' }}

        store.updateStateProperty(homeroomEvent)
        expect(store.homeroomPrefix).toBe('Big Time Test')

        store.updateStateProperty(periodsEvent)
        expect(store.numberOfPeriods).toBe('10')
    })
})