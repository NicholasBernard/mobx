import React from 'react'
import { inject, observer } from 'mobx-react'
import { TextField } from 'react-md'
import PageHeader from '../../common/PageHeader'
import NavButtons from '../../common/buttons/NavButtons'

@inject('PeriodsPrefixStore')
@observer
export default class PeriodsPrefix extends React.Component {
    componentWillMount() {
        this.props.PeriodsPrefixStore.fetchData()
    }

    render() {
        const { homeroomPrefix, numberOfPeriods, saveData, updateStateProperty } = this.props.PeriodsPrefixStore
        return (
            <div>
                <PageHeader headerText='Periods & Prefix' />
                    <p>First we will determine how many periods your school schedule will contain.</p>
                    <TextField               
                        className="md-cell"
                        floating={true}
                        id="numberOfPeriods" 
                        label='Number of Periods'
                        name='numberOfPeriods'
                        onBlur={() => saveData()}
                        onChange={(value, e) => updateStateProperty(e)}
                        value={numberOfPeriods} />
                    <p>You also have the opportunity to decide on a prefix for your homeroom course.</p>
                    <TextField
                        className="md-cell"
                        floating={true}
                        id="homeroomPrefix"
                        label='Homeroom Prefix'
                        name='homeroomPrefix'
                        onBlur={() => saveData()}
                        onChange={(value, e) => updateStateProperty(e)}
                        value={homeroomPrefix} />
                    <NavButtons 
                        prevPage="/index"
                        nextPage="/periodNames" />
            </div>
        )
    }
}