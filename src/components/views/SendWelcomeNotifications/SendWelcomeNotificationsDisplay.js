import React from 'react';
import PageHeader from '../../common/PageHeader'
import NavButtons from '../../common/buttons/NavButtons'
import EmailSelectors from './EmailSelectors'

const SendWelcomeNotificationsDisplay = () => (
    <div>
        <PageHeader headerText="Send Welcome Notifications" />
        <p>You are now ready to send out notifications welcoming your users to the application.  These emails will include a temporary password to log on initially.  Once logged on they can change their individual passwords."</p>
        <p>To finish this process, select the users you'd like to email from the list below.  You can choose any or all options.  Once logged in, an admin can finish this process from inside the application.</p>
        <EmailSelectors />
        <NavButtons 
            prevPage="/setTechnicalContact"
            nextPage="/finish" />
    </div>
)

export default SendWelcomeNotificationsDisplay