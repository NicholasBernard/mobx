import { shallow } from 'enzyme'
import React from 'react'

import SendWelcomeNotificationsDisplay from '../SendWelcomeNotificationsDisplay'

describe('SendWelcomeNotificationsDisplay', () => {
    const wrapper = shallow(        
        <SendWelcomeNotificationsDisplay />        
    )

    it('renders correctly', () => {
        expect(wrapper).toMatchSnapshot()
    })
})