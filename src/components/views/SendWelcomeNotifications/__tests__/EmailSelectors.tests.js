import React from 'react';
import { shallow } from 'enzyme';
import EmailSelectors from '../EmailSelectors';

describe('EmailSelectors', () => {
    const store = {
        sendWelcomeNotifications: jest.fn()
    }

    const wrapper = shallow(
        <EmailSelectors />
    );

    it('should set the state correctly when the handleOnChange function is called', () => {
        wrapper.instance().handleOnChange(0);
        expect(wrapper.instance().state.showOtherEmailInput).toBe(true);

        wrapper.instance().handleOnChange(3);
        expect(wrapper.instance().state.showOtherEmailInput).toBe(false);
        expect(wrapper.instance().state.emailObject).toEqual({emailGroup: 3});
    });

    it('should set the state correctly when the handleOtherEmail function is called', () => {
        wrapper.instance().handleOtherEmail('Testing');
        expect(wrapper.instance().state.otherEmailObject).toEqual({emailAddress: 'Testing', emailGroup: 0});
    });

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot();
    });

    it('should fire handleOnChange event as user clicks radio buttons', () => {
        const spy = jest.spyOn(wrapper.instance(), 'handleOnChange')
        wrapper.find('#email-selection-controls').simulate('change', 1);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should fire sendEmail event on button click', () => {
        const spy = jest.spyOn(wrapper.instance(), 'sendEmail')
        wrapper.find('#send-email').simulate('click');
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should call handleOtherEmail function onChange of #other-email-address', () => {
        wrapper.setState({ showOtherEmailInput: true})
        const spy = jest.spyOn(wrapper.instance(), 'handleOtherEmail')
        wrapper.find('#other-email-address').simulate('change')
        expect(spy).toHaveBeenCalledTimes(1)
    });
});