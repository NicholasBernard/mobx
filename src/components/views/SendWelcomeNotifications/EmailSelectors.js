import React from 'react'
import { Button, SelectionControlGroup, TextField } from 'react-md'
import axios from 'axios'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export default class EmailSelectors extends React.Component {
    constructor(){
        super()
        this.state = {
            showOtherEmailInput: false,
            emailObject: {
                emailGroup: 1
            },
            otherEmailObject: {
                emailAddress: '',
                emailGroup: 0
            }
        }
    }

    handleOnChange = (value) => {
        const emailGroup = parseInt(value, 10);
        
        if(emailGroup ===  0){
            this.setState({
                showOtherEmailInput: true
            })
        }else{
            const emailObject = this.state.emailObject;
            const updatedEmailObject = {
                ...emailObject,
                emailGroup: emailGroup
            };
            this.setState((state) => ({
                showOtherEmailInput: false,
                emailObject: updatedEmailObject
            }));
        }
    }

    handleOtherEmail = (value) => {
        const otherEmailObject = this.state.otherEmailObject;
        const updatedEmailObject = {
            ...otherEmailObject,
            emailAddress: value
        }
        this.setState({
            otherEmailObject: updatedEmailObject
        })
    }

    sendEmail = () => {
        /* istanbul ignore next */
        if(this.state.showOtherEmailInput){
            axios.post(`${ROOT_URL}email/send/`, this.state.otherEmailObject, createESAuthToken())
        }else{
            axios.post(`${ROOT_URL}email/send/`, this.state.emailObject, createESAuthToken())
        }
    }

    render() {
        return (
            <div>
                <SelectionControlGroup
                    id='email-selection-controls'
                    name='email-selection-controls'
                    onChange={(value) => this.handleOnChange(value)}
                    type='radio'
                    controls={
                        [
                            {
                                label: 'Admins',
                                value: '1'
                            },
                            {
                                label: 'Staff',
                                value: '2'
                            },
                            {
                                label: 'Staff & Students',
                                value: '3'
                            },
                            {
                                label: 'Other',
                                value: '0'
                            },
                        ]
                    } />
                {
                    this.state.showOtherEmailInput ?
                    <TextField
                        className='md-cell'
                        id='other-email-address'
                        label='Other Email Address'
                        name='other-email-address'
                        onChange={(value) => this.handleOtherEmail(value)} />
                    : null
                }
                <Button                
                    className='md-cell'
                    id='send-email'
                    onClick={() => this.sendEmail()}
                    primary
                    raised >
                        Send Email
                </Button>
            </div>
        )
    }
}