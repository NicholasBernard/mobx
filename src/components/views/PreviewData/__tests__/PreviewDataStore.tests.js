import React from 'react'
import {PreviewDataStore} from '../PreviewDataStore'

describe('PreviewDataStore', () => {
    const store = new PreviewDataStore()

    it('calls functions correctly from savePreviewData function', () => {
        const value = 'Testing'
        const handleSelectionSpy = jest.spyOn(store, 'handleSelectionChange')
        const createObjectSpy = jest.spyOn(store, 'createObjectToSave')
        const resetSpy = jest.spyOn(store, 'resetDataAfterSave')

        store.savePreviewData(value)
        expect(store.handleSelectionChange).toHaveBeenCalledTimes(1)
        expect(store.handleSelectionChange).toHaveBeenCalledWith(value)
        expect(createObjectSpy).toHaveBeenCalledTimes(1)
        expect(resetSpy).toHaveBeenCalledTimes(1)
    })

    it('updates observables correctly on createSaveObject action', () => {
        store.selectedOption = '2'
        store.createObjectToSave()
        expect(store.objectToSave).toEqual({
            CourseFormatId: store.selectedOption
        })
    })

    it('updates observables correctly on handleSelectionChange action', () => {
        store.handleSelectionChange('3')
        expect(store.selectedOption).toBe('3')
    })

    it('updates observables correctly on resetDataAfterSave action', () => {
        store.objectToSave = {
            value: 'This is going away!'
        }
        store.resetDataAfterSave()
        expect(store.objectToSave).toEqual({})
    })

    it('updates observables correctly on setStoreItems action', () => {
        store.setStoreItems(5)
        expect(store.selectedOption).toBe('5')
    })
})