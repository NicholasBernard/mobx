import React from 'react'
import { shallow, mount } from 'enzyme'
import { wrappedComponent } from 'mobx'
import PreviewDataDisplay from '../PreviewDataDisplay'

describe('PreviewDataDisplay', () => {
    let store = {
        fetchData: jest.fn(),
        savePreviewData: jest.fn(),
        selectedOption: ''
    }

    const wrapper = shallow(        
        <PreviewDataDisplay.wrappedComponent
            PreviewDataStore={ store } />        
    )

    const teacherEvent = {target: {name: 'teacherName', value: 'Bernard'}}
    const departmentEvent = {target: {name: 'department', value: 'Math'}}
    const roomEvent = {target: {name: 'room', value: '101'}}

    it('should render correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.fetchData).toHaveBeenCalledTimes(1)
    })

    it('should set the state correctly when onInputChange function is called', () => {
        wrapper.instance().onInputChange(teacherEvent)
        expect(wrapper.instance().state.teacherName).toBe(teacherEvent.target.value)

        wrapper.instance().onInputChange(roomEvent)
        expect(wrapper.instance().state.room).toBe(roomEvent.target.value)
    })

    it('should return correct form when the renderDisplayInformation function is called', () => {
        wrapper.setState({
            department: 'Math',
            room: '101',
            teacherName: 'Bernard'
        })

        store.selectedOption = '0'
        expect(wrapper.instance().renderDisplayInformation()).toBe('Bernard - 101 - Math')

        store.selectedOption = '1'
        expect(wrapper.instance().renderDisplayInformation()).toBe('Math - 101 - Bernard');

        store.selectedOption = '2'
        expect(wrapper.instance().renderDisplayInformation()).toBe('Bernard - Math')

        store.selectedOption = '3'
        expect(wrapper.instance().renderDisplayInformation()).toBe('Math - Bernard')

        store.selectedOption = 'MoonManTalk'
        expect(wrapper.instance().renderDisplayInformation()).toBe('Loading...')
    })

    it('calls the savePreviewData function onChange of selection control', () => {
        wrapper.find('#email-selection-controls').simulate('change')
        expect(store.savePreviewData).toHaveBeenCalledTimes(1)
    })
})