import React from 'react'
import { inject, observer } from 'mobx-react'
import PageHeader from '../../common/PageHeader'
import NavButtons from '../../common/buttons/NavButtons'
import { SelectionControlGroup, TextField } from 'react-md'

@inject('PreviewDataStore')
@observer
export default class PreviewDataDisplay extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            department: 'Math',
            room: '101',
            teacherName: 'Smith'
        }
    }

    componentWillMount() {
        this.props.PreviewDataStore.fetchData()
    }

    onInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    renderDisplayInformation() {
        switch(this.props.PreviewDataStore.selectedOption){
            case '0':
                return this.state.teacherName + ' - ' + this.state.room + ' - ' + this.state.department;
            case '1':
                return this.state.department + ' - ' + this.state.room + ' - ' + this.state.teacherName;
            case '2':
                return this.state.teacherName + ' - ' + this.state.department;
            case '3':
                return this.state.department + ' - ' + this.state.teacherName;
            default:
                return 'Loading...';
        }
    }

    renderTextField(id, label) {
        return (
            <TextField
                className='md-cell'
                id={id}
                label={label}
                name={id}
                onChange={(value, event) => this.onInputChange(event)}
                value={this.state[id]} />
        )
    }

    render(){
        const { savePreviewData, selectedOption } = this.props.PreviewDataStore
        return(
            <div>
                <PageHeader headerText="Preview Your Data" />
                <p>We are almost finished!  Below you can see an example of what a day might look like from with inside the application.  Please enter some fake data in the inputs marked Teacher Name, Department and Room.  You'll notice that as you change the radio buttons, the way that information is displayed is changed as well.  Please select whichever option would be beneficial to your school.</p>
                <SelectionControlGroup
                    id='email-selection-controls'
                    name='email-selection-controls'
                    onChange={(value) => savePreviewData(value)}
                    type='radio'
                    value={selectedOption}
                    controls={
                        [
                            {
                                label: 'Teacher Name - Room - Department',
                                value: '0'
                            },
                            {
                                label: 'Department - Room - Teacher Name',
                                value: '1'
                            },
                            {
                                label: 'Teacher Name - Department',
                                value: '2'
                            },
                            {
                                label: 'Department - Teacher Name',
                                value: '3'
                            },
                        ]
                    }
                />
                <div className='preview-inputs'>
                    {this.renderTextField('teacherName', 'Teacher Name')}
                    {this.renderTextField('department', 'Department')}
                    {this.renderTextField('room', 'Room')}                    
                </div>
                <div className='preview-day'>
                    <p>Monday</p>
                    <p>{this.renderDisplayInformation()}</p>
                    <p>Instructor/Room</p>
                    <p>Smith, A/101</p>
                    <p>Scheduler/Room</p>
                    <p>Smith, A/101</p>
                    <p>Appointment Type</p>
                    <p>Extra Help</p>
                </div>
                <NavButtons
                    prevPage="/importGradeSettings"
                    nextPage="/importData"/>
            </div>
        )
    }
}