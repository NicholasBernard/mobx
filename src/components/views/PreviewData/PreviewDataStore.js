import axios from 'axios'
import { action, observable } from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class PreviewDataStore {
    @observable selectedOption
    @observable objectToSave = {}

    fetchData() {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}preview/getcourseformat`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    savePreviewData = (value) => {
        this.handleSelectionChange(value)
        this.createObjectToSave()
        /* istanbul ignore next */
        axios.put(`${ROOT_URL}preview/save/`, this.objectToSave, createESAuthToken())
        this.resetDataAfterSave()
    }

    @action createObjectToSave = () => {
        this.objectToSave = {
            CourseFormatId: this.selectedOption
        }
    }

    @action handleSelectionChange = (value) => {
        this.selectedOption = value
    }

    @action resetDataAfterSave = () => {
        this.objectToSave = {}
    }

    @action setStoreItems = (data) => {
        this.selectedOption = JSON.stringify(data)
    }
}

export default new PreviewDataStore()