import axios from 'axios'
import { action, observable } from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class ImportGradeSettingsStore {
    @observable emailAddressToNotify = ''
    @observable gradeTypeId = -1
    @observable isDirty = false
    @observable objectToSave = {}

    fetchData() {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}importgradessettings/get/`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    saveImportGradeSettings = (value, e) => {
        this.handleInputChange(value, e)
        this.createObjectToSave()
        /* istanbul ignore next */
        axios.put(`${ROOT_URL}importgradessettings/save/`, this.objectToSave, createESAuthToken())
        this.resetStoreAfterSave()
    }

    saveOnBlur = () => {        
        if(this.isDirty){
            this.createObjectToSave()
            /* istanbul ignore next */
            axios.put(`${ROOT_URL}importgradessettings/save/`, this.objectToSave, createESAuthToken())
            this.resetStoreAfterSave()
        }
    }

    @action createObjectToSave = () => {
        this.objectToSave = {
            emailAddressToNotify: this.emailAddressToNotify,
            gradeTypeId: this.gradeTypeId
        }
    }

    @action handleInputChange = (value, e) => {
        if(e.target.name === 'emailAddressToNotify'){
            this.emailAddressToNotify = value
            this.isDirty = true
            return
        }
        this.gradeTypeId = value
    }

    @action resetStoreAfterSave = () => {
        this.objectToSave = {}
        this.isDirty = false
    }

    @action setStoreItems = (data) => {
        this.emailAddressToNotify = data.emailAddressToNotify
        this.gradeTypeId = data.gradeTypeId
    }
}

export default new ImportGradeSettingsStore()