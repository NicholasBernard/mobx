import React from 'react'
import { ImportGradeSettingsStore } from '../ImportGradeSettingsStore'

describe('ImportGradeSettingsStore', () => {
    const store = new ImportGradeSettingsStore()
    const createObjectSpy = jest.spyOn(store, 'createObjectToSave')
    const handleInputSpy = jest.spyOn(store, 'handleInputChange')
    const resetStoreSpy = jest.spyOn(store, 'resetStoreAfterSave')

    store.emailAddressToNotify = 'Test@TestEmail.com'
    store.gradeTypeId = 1

    it('calls functions correctly from the saveOnBlur function', () => {
        store.isDirty = false
        store.saveOnBlur()

        expect(createObjectSpy).toHaveBeenCalledTimes(0)
        expect(resetStoreSpy).toHaveBeenCalledTimes(0)

        store.isDirty = true
        store.saveOnBlur()

        expect(createObjectSpy).toHaveBeenCalledTimes(1)
        expect(resetStoreSpy).toHaveBeenCalledTimes(1)
    })

    it('calls functions correctly from the saveImportGradeSettings function', () => {
        const value = 'BigTimeTest@Email.com'
        const event = {target: { name: 'emailAddressToNotify'}}

        store.saveImportGradeSettings(value, event)

        expect(handleInputSpy).toHaveBeenCalledTimes(1)
        expect(handleInputSpy).toHaveBeenCalledWith(value, event)
        expect(createObjectSpy).toHaveBeenCalledTimes(2)
        expect(resetStoreSpy).toHaveBeenCalledTimes(2)
    })

    it('updates observable correctly on createSaveObject action', () => {
        store.createObjectToSave()
        expect(store.objectToSave).toEqual({
            emailAddressToNotify: store.emailAddressToNotify,
            gradeTypeId: store.gradeTypeId
        })
    })

    it('updates observable correctly on handleInputChange action', () => {
        const emailEvent = { target: { name: 'emailAddressToNotify', value: 'anotherFakeEmail@Testing.com' }}
        const gradeEvent = { target: { name:'gradeTypeId' }}

        store.handleInputChange(0, gradeEvent)
        expect(store.gradeTypeId).toBe(0)
        expect(store.isDirty).toBe(false)

        store.handleInputChange('anotherFakeEmail@Testing.com', emailEvent)
        expect(store.emailAddressToNotify).toEqual('anotherFakeEmail@Testing.com')
        expect(store.isDirty).toBe(true)
    })

    it('updates observable correctly on setStoreAfterSave action', () => {
        store.resetStoreAfterSave()
        expect(store.objectToSave).toEqual({})
        expect(store.isDirty).toBe(false)
    })

    it('updates observable correctly on setStoreItems action', () => {
        store.emailAddressToNotify = ''
        store.gradeTypeId = -1

        const data = {
            emailAddressToNotify: 'OneLastTime@Emails.com',
            gradeTypeId: 1
        }
        
        store.setStoreItems(data)
        expect(store.emailAddressToNotify).toBe(data.emailAddressToNotify)
        expect(store.gradeTypeId).toBe(data.gradeTypeId)
    })
})