import React from 'react'
import { shallow } from 'enzyme'
import { wrappedComponent } from 'mobx'
import ImportGradeSettingsDisplay from '../ImportGradeSettingsDisplay'

describe('ImportGradeSettingsDisplay', () => {
    const store = {
        emailAddressToNotify: 'Mark@EnrichingStudents.com',
        fetchData: jest.fn(),
        gradeTypeId: 0,
        handleInputChange: jest.fn(),
        saveImportGradeSettings: jest.fn(), 
        saveOnBlur: jest.fn()
    }

    const wrapper = shallow(        
        <ImportGradeSettingsDisplay.wrappedComponent
            ImportGradeSettingsStore={ store } />        
    )

    it('renders correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.fetchData).toHaveBeenCalledTimes(1)
    })

    it('calls saveOnBlur function onBlur of #email-input', () => {
        wrapper.find('#email-input').simulate('blur')
        expect(store.saveOnBlur).toHaveBeenCalledTimes(1)
    })

    it('calls handleInputChange function onChange of #email-input', () => {
        wrapper.find('#email-input').simulate('change')
        expect(store.handleInputChange).toHaveBeenCalledTimes(1)
    })

    it('calls saveImportGradeSettings function onChange of #select-periods', () => {
        wrapper.find('#select-periods').simulate('change')
        expect(store.saveImportGradeSettings).toHaveBeenCalledTimes(1)
    })
})