import React from 'react'
import { inject, observer } from 'mobx-react'
import { SelectField, TextField } from 'react-md'
import PageHeader from '../../common/PageHeader'
import NavButtons from '../../common/buttons/NavButtons'

@inject('ImportGradeSettingsStore')
@observer
export default class ImportGradeSettingsDisplay extends React.Component{
    componentDidMount = () => {
        this.props.ImportGradeSettingsStore.fetchData()
    }

    render(){
        const menuItems = [
            {
                label: "Select",
                value: -1
            },
            {
                label: "Letter Grades",
                value: 0
            },
            {
                label: "Number Grades",
                value: 1
            }
        ]
        const { emailAddressToNotify, gradeTypeId, handleInputChange, saveImportGradeSettings, saveOnBlur } = this.props.ImportGradeSettingsStore
        return(
            <div>
                <PageHeader headerText="Import Grade Settings" />
                <p>Next we need to store an email address to notify when grades are imported.  If your school uses the automated grade import feature, this person will be notified when the process has been completed.</p>
                <TextField
                    className="md-cell"
                    floating={true}
                    id="email-input"
                    label="Enter email address"
                    name='emailAddressToNotify'
                    onBlur={() => saveOnBlur()}
                    onChange={(value, e) => handleInputChange(value, e)}
                    value={emailAddressToNotify} />
                <p>Now we need to determine how your school will measure grades.  Grades can either be letters (A+,A,A-,...F,F-), or numbers (0 to 100).  Please select the types of grades you will be importing into Enriching Students.</p>
                <SelectField 
                    className="md-cell"
                    id="select-periods"
                    menuItems={menuItems}
                    name='gradeTypes'
                    onChange={(value, i, e) => saveImportGradeSettings(value, e)}
                    sameWidth
                    value={gradeTypeId} />
                <NavButtons 
                    prevPage="/attendanceSettings"
                    nextPage="/previewData"/>
            </div>
        )
    }
}