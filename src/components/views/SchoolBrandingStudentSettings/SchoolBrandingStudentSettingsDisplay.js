import React from 'react'
import { inject, observer } from 'mobx-react'
import PageHeader from '../../common/PageHeader'
import NavButtons from '../../common/buttons/NavButtons'
import { TextField, SelectionControl } from 'react-md'

@inject('SchoolBrandingStudentSettingsStore')
@observer
export default class SchoolBrandingStudentEmailSettingsDisplay extends React.Component{
    componentDidMount = () => {
        this.props.SchoolBrandingStudentSettingsStore.fetchData()
    }

    render(){
        const { handleInputChange, saveOnBlur, saveOnChange, schoolBranding, sendCommentsInStudentEmails, studentCanOverwriteHomeroomAppt } = this.props.SchoolBrandingStudentSettingsStore
        return(
            <div>
                <PageHeader headerText="School Branding & Student Settings" />
                <p>Next we are going to set the name of your Flex time within Enriching Students.</p>
                <TextField
                    className="md-cell"
                    floating={true}
                    id="flex-time-name"
                    label="Flex Time"
                    name="schoolBranding"
                    onBlur={() => saveOnBlur()}
                    onChange={(value, e) => handleInputChange(e)}
                    value={schoolBranding} />
                <p>Also, you can set a permission to allow students to view a teachers comment when they are emailed.  By default the students only receive their schedule.</p>
                <SelectionControl
                    checked={sendCommentsInStudentEmails}
                    className="md-cell"
                    id="comments-student-emails"            
                    label="Send comments in student emails?"
                    labelBefore={true}
                    name="sendCommentsInStudentEmails"
                    onChange={(checked, e) => saveOnChange(e)}
                    type="checkbox" />
                <p>Finally, you can set a permission to allow your students to overwrite a homeroom appointment.</p>
                <SelectionControl
                    checked={studentCanOverwriteHomeroomAppt}
                    className="md-cell"
                    id="student-can-overwrite-homeroom-appointment"            
                    label="Student can overwrite homeroom appointment?"
                    labelBefore={true}
                    name="studentCanOverwriteHomeroomAppt"
                    onChange={(checked, e) => saveOnChange(e)}
                    type="checkbox" />
                <NavButtons
                    prevPage="/graduationSettings"
                    nextPage="/attendanceSettings" />
            </div>
        )
    }
}