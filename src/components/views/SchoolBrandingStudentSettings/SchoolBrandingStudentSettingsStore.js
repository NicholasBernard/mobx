import axios from 'axios'
import { action, observable } from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class SchoolBrandingStudentSettingsStore {
    @observable schoolBranding = ''
    @observable sendCommentsInStudentEmails = false
    @observable studentCanOverwriteHomeroomAppt = false
    @observable objectToSave = {}
    @observable isDirty = false

    fetchData() {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}schoolbrandstudentemail/get/`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    saveOnBlur = () => {
        if(this.isDirty){
            this.createObjectToSave()
            /* istanbul ignore next */
            axios.put(`${ROOT_URL}schoolbrandstudentemail/save/`, this.objectToSave, createESAuthToken())
            this.resetStoreAfterSave()
        }
    }
    
    saveOnChange = (e) => { 
        this.handleInputChange(e)
        this.createObjectToSave()
        /* istanbul ignore next */
        axios.put(`${ROOT_URL}schoolbrandstudentemail/save/`, this.objectToSave, createESAuthToken())
        this.resetStoreAfterSave()
    }

    @action createObjectToSave = () => {
        this.objectToSave = {
            schoolBranding: this.schoolBranding,
            sendCommentsInStudentEmails: this.sendCommentsInStudentEmails,
            studentCanOverwriteHomeroomAppt: this.studentCanOverwriteHomeroomAppt
        }
    }

    @action handleInputChange = (e) => {
        if(e.target.name === 'schoolBranding'){ //we are in the input
            this[e.target.name] = e.target.value
            this.isDirty = true
            return
        }
        
        this[e.target.name] = e.target.checked
    }

    @action resetStoreAfterSave =() => {
        this.objectToSave = {}
        this.isDirty = false
    }

    @action setStoreItems = (data) => {
        this.schoolBranding = data.schoolBranding
        this.sendCommentsInStudentEmails = data.sendCommentsInStudentEmails
        this.studentCanOverwriteHomeroomAppt = data.studentCanOverwriteHomeroomAppt
    }
}

export default new SchoolBrandingStudentSettingsStore()