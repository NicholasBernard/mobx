import React from 'react'
import { shallow, mount } from 'enzyme'
import { wrappedComponent } from 'mobx'
import SchoolBrandingStudentSettingsDisplay from '../SchoolBrandingStudentSettingsDisplay'

describe('SchoolBrandingStudentSettingsDisplay', () => {
    const store = {
        fetchData: jest.fn(),
        handleInputChange: jest.fn(),
        saveOnBlur: jest.fn(),
        saveOnChange: jest.fn(),
        schoolBranding: '',
        sendCommentsInStudentEmails: false,
        studentCanOverwriteHomeroomAppt: false 
    }

    const wrapper = shallow(        
        <SchoolBrandingStudentSettingsDisplay.wrappedComponent
            SchoolBrandingStudentSettingsStore={ store } />        
    )

    it('renders correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.fetchData).toHaveBeenCalledTimes(1)
    })

    it('calls the saveOnBlur function onBlur of #flex-time-name', () => {
        wrapper.find('#flex-time-name').simulate('blur')
        expect(store.saveOnBlur).toHaveBeenCalledTimes(1)
    })

    it('calls the handleInputChange function onChange of #flex-time-name', () => {
        const event = { target: { name: 'schoolBranding', value:'More tests'}}
        wrapper.find('#flex-time-name').simulate('change', event)
        expect(store.handleInputChange).toHaveBeenCalledTimes(1)
    })

    it('calls the saveOnChange function onChange of selection controls', () => {
        const emailEvent = { target: { name: 'sendCommentsInStudentEmails', checked: true}}
        const homeroomEvent = { target: { name: 'studentCanOverwriteHomeroomAppt', checked: true}}

        wrapper.find('#comments-student-emails').simulate('change', emailEvent)
        expect(store.saveOnChange).toHaveBeenCalledTimes(1)

        wrapper.find('#student-can-overwrite-homeroom-appointment').simulate('change', homeroomEvent)
        expect(store.saveOnChange).toHaveBeenCalledTimes(2)
    })
})