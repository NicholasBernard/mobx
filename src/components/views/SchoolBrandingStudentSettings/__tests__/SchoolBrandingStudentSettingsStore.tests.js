import React from 'react'
import {SchoolBrandingStudentSettingsStore} from '../SchoolBrandingStudentSettingsStore'

describe('SchoolBrandingStudentSettingsStore', () => {
    const store = new SchoolBrandingStudentSettingsStore()
    const testEvent = { target: { name: 'schoolBranding', value: 'Test Branding!'}}
    const createObjectSpy = jest.spyOn(store, 'createObjectToSave')
    const handleInputSpy = jest.spyOn(store, 'handleInputChange')
    const resetSpy = jest.spyOn(store, 'resetStoreAfterSave')

    it('calls functions correctly from the saveOnChange function', () => {
        store.isDirty = false
        store.saveOnBlur()
        expect(createObjectSpy).toHaveBeenCalledTimes(0)
        expect(resetSpy).toHaveBeenCalledTimes(0)

        store.isDirty = true
        store.saveOnBlur()
        expect(createObjectSpy).toHaveBeenCalledTimes(1)
        expect(resetSpy).toHaveBeenCalledTimes(1)
    })

    it('calls functions correctly from the saveOnChange function', () => {
        store.saveOnChange(testEvent)

        expect(handleInputSpy).toHaveBeenCalledTimes(1)
        expect(handleInputSpy).toHaveBeenCalledWith(testEvent)
        expect(createObjectSpy).toHaveBeenCalledTimes(2)
        expect(resetSpy).toHaveBeenCalledTimes(2)
    })

    it('set observables correctly on createSaveObject action', () => {
        store.schoolBranding = 'Test School Name'
        store.sendCommentsInStudentEmails = false
        store.studentCanOverwriteHomeroomAppt = false

        store.createObjectToSave()
        expect(store.objectToSave).toEqual({
            schoolBranding: 'Test School Name',
            sendCommentsInStudentEmails: false,
            studentCanOverwriteHomeroomAppt: false
        })
    })

    it('set observables correctly on handleInputChange action with schoolBranding event', () => {
        store.isDirty = false
        const event = { target: { name: 'schoolBranding', value:'Big Time Test Baby!'}}
        
        store.handleInputChange(event)
        expect(store.schoolBranding).toBe(event.target.value)
        expect(store.isDirty).toBe(true)
    })

    it('set observables correctly on handleInputChange action with checked event', () => {
        store.isDirty = false
        const event = { target: { name: 'sendCommentsInStudentEmails', checked: true}}
        
        store.handleInputChange(event)
        expect(store.sendCommentsInStudentEmails).toBe(event.target.checked)
        expect(store.isDirty).toBe(false)
    })

    it('set observables correctly on resetStoreAfterSave action', () => {
        store.isDirty = true
        store.objectToSave = {
            value: 'This shouldnt be here!'
        }
        
        store.resetStoreAfterSave()
        expect(store.objectToSave).toEqual({})
        expect(store.isDirty).toBe(false)
    })

    it('set observables correctly on setStoreItems action', () => {
        const data = {
            schoolBranding: 'My Test School',
            sendCommentsInStudentEmails: true,
            studentCanOverwriteHomeroomAppt: true
        }
        
        store.setStoreItems(data)
        expect(store.schoolBranding).toBe(data.schoolBranding)
        expect(store.sendCommentsInStudentEmails).toBe(data.sendCommentsInStudentEmails)
        expect(store.studentCanOverwriteHomeroomAppt).toBe(data.studentCanOverwriteHomeroomAppt)
    })
})