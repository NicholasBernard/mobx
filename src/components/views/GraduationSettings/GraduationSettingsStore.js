import axios from 'axios'
import { observable, action } from 'mobx'
import { createESAuthToken, ROOT_URL} from '../../../services/baseService'

export class GraduationSettingsStore {
    @observable endYear = ''
    @observable startYear = ''
    @observable objectToSave = {}
    @observable isDirty = false

    fetchData() {
        /* istanbul ignore next */
        axios.get(`${ROOT_URL}graduationyears/get/`, createESAuthToken())
        .then(response => {this.setStoreItems(response.data)})
    }

    saveGraduationSettings() {
        /* istanbul ignore next */
        if(this.isDirty){
            this.createSaveObject()        
            axios.put(`${ROOT_URL}graduationyears/save/`, this.saveObject, createESAuthToken())
            this.resetStateAfterSave()
        }        
    }

    @action createObjectToSave = () => {
        const objectToSave = {
            startYear: this.startYear,
            endYear: this.endYear
        }
        this.objectToSave = objectToSave
    }

    @action resetStateAfterSave = () => {
        this.isDirty = false
        this.objectToSave = {}
    }

    @action setStoreItems = (store) => {
        this.endYear = store.endYear
        this.startYear = store.startYear
    } 

    @action updateGraduationYear = (e) => {
        this[e.target.name] = e.target.value
        this.isDirty = true
    }
}

export default new GraduationSettingsStore()