import React from 'react'
import { inject, observer } from 'mobx-react'
import PageHeader from '../../common/PageHeader'
import NavButtons from '../../common/buttons/NavButtons'
import { TextField } from 'react-md'

@inject('GraduationSettingsStore')
@observer
export default class GraduationSettings extends React.Component {
    componentWillMount() {
        this.props.GraduationSettingsStore.fetchData()
    }

    render(){
        const { endYear, startYear, saveGraduationSettings, updateGraduationYear } = this.props.GraduationSettingsStore
        return(
            <div className='graduation-settings-container'>
                <PageHeader headerText="Graduation Settings" />
                <p>Please enter the Start and End years of graduation.</p>                
                <TextField                    
                    className="md-cell"
                    floating={true}
                    id="startYear" 
                    label='Graduation Start Year'
                    name="startYear"
                    onBlur={() => saveGraduationSettings()}
                    onChange={(value, e) => updateGraduationYear(e)}
                    value={startYear} />
                <TextField                    
                    className="md-cell"
                    floating={true}
                    id="endYear" 
                    label='Graduation End Year'
                    name="endYear"
                    onBlur={() => saveGraduationSettings()}
                    onChange={(value, e) => updateGraduationYear(e)}
                    value={endYear} />
                <NavButtons 
                    prevPage="/blockDateSettings"
                    nextPage="/schoolBrandingStudentSettings" />
            </div>
        )
    }
}