import React from 'react'
import { shallow, mount } from 'enzyme'
import { wrappedComponent } from 'mobx'
import GraduationSettingsDisplay from '../GraduationSettingsDisplay'

describe('GraduationSettings', () => {
    const store = {
        endYear: '',
        startYear: '',
        fetchData: jest.fn(),        
        saveGraduationSettings: jest.fn(),
        updateGraduationYear: jest.fn()
    }

    const wrapper = shallow(        
        <GraduationSettingsDisplay.wrappedComponent
            GraduationSettingsStore={store} />        
    )

    it('renders correctly', () => {
        expect(wrapper).toMatchSnapshot()
        expect(store.fetchData).toHaveBeenCalledTimes(1)
    })

    it('calls the saveGraduationSettings function from each textfield onBlur event', () => {
        wrapper.find('#startYear').simulate('blur')
        expect(store.saveGraduationSettings).toHaveBeenCalledTimes(1)

        wrapper.find('#endYear').simulate('blur')
        expect(store.saveGraduationSettings).toHaveBeenCalledTimes(2)
    })

    it('calls the updateGraduationYear function from each textfield onChange event', () => {
        const startYearEvent = { target: { name: 'startYear', value: '2000'}}
        const endYearEvent = { target: { name: 'endYear', value: '2004'}}

        wrapper.find('#startYear').simulate('change', startYearEvent)
        expect(store.updateGraduationYear).toHaveBeenCalledTimes(1)

        wrapper.find('#endYear').simulate('change', endYearEvent)
        expect(store.updateGraduationYear).toHaveBeenCalledTimes(2)
    })
})