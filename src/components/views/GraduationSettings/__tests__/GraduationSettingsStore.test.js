import React from 'react'
import {GraduationSettingsStore} from '../GraduationSettingsStore'

describe('GraduationSettingsStore', () => {
    const graduationStore = new GraduationSettingsStore()

    it('updates saveObject observable correctly on createSaveObject action', () => {
        graduationStore.endYear = '2000'
        graduationStore.startYear = '1996'
        graduationStore.saveObject = {}

        graduationStore.createObjectToSave()

        expect(graduationStore.objectToSave).toEqual({
            startYear: '1996',
            endYear: '2000'
        })
    })

    it('updates observables correctly on resetStateAfterSave action', () => {
        graduationStore.isDirty = true
        graduationStore.objectToSave = {
            endYear: '2000',
            startYear: '1996'
        }

        graduationStore.resetStateAfterSave()

        expect(graduationStore.isDirty).toBe(false)
        expect(graduationStore.objectToSave).toEqual({})        
    })

    it('updates observables correctly on setStoreItems action', () => {
        const store = {
            endYear: '1996',
            startYear: '1992'
        }

        graduationStore.setStoreItems(store)

        expect(graduationStore.startYear).toBe(store.startYear)
        expect(graduationStore.endYear).toBe(store.endYear)
    })

    it('updates observables correctly on updateGraduationYear action', () => {
        const eventStartYear = {target: { name: 'startYear', value: '2020'}}
        const eventEndYear = {target: { name: 'endYear', value: '2024'}}        

        graduationStore.updateGraduationYear(eventStartYear)
        graduationStore.updateGraduationYear(eventEndYear)

        expect(graduationStore.startYear).toBe(eventStartYear.target.value)
        expect(graduationStore.endYear).toBe(eventEndYear.target.value)
        expect(graduationStore.isDirty).toBe(true)
    })
})