export const ROOT_URL = 'http://preflightimport.enrichingstudents.com/v1.0/'

export const createESAuthToken = () => {
    /* istanbul ignore next */
    return {
        headers: {
            //"ESAuthToken": "wueiosdzm:1;qdjHDnmxadf:otF-aQVt1wgEjY0Ixvzrg4BHJUaoTdd0h8M9sQ__"  //comment out this line when using on the dev site
            "ESAuthToken": localStorage.getItem("esAuthToken")  // comment out this line when developing locally
            //"ESAuthToken": "thisshouldntwork" //Bad token
        }
    }
}