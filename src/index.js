import React from 'react';
import { render } from 'react-dom';
import WebFontLoader from 'webfontloader';
import './index.css';
import { Provider } from 'mobx-react'
import { useStrict } from 'mobx'
import { BrowserRouter, } from 'react-router-dom'


import SchoolCalendarSettingsStore from './components/views/SchoolCalendarSettings/SchoolCalendarSettingsStore'
import TechnicalContactStore from './components/views/TechnicalContact/TechnicalContactStore'

import SetAdminsStore from './components/views/SetAdmins/SetAdminsStore'

import ImportDataStore from './components/views/ImportData/ImportDataStore'

import PreviewDataStore from './components/views/PreviewData/PreviewDataStore'

import ImportGradeSettingsStore from './components/views/ImportGradeSettings/ImportGradeSettingsStore'


import AttendanceSettingsStore from './components/views/AttendanceSettings/AttendanceSettingsStore'

import SchoolBrandingStudentSettingsStore from './components/views/SchoolBrandingStudentSettings/SchoolBrandingStudentSettingsStore'

import BlockDateSettingsStore from './components/views/BlockDateSettings/BlockDateSettingsStore'

import AppointmentTypeSettingsStore from './components/views/AppointmentTypeSettings/AppointmentTypeSettingsStore'

import PrebookSettingsPeriodsStore from './components/views/PrebookSettingsPeriods/PrebookSettingsPeriodsStore'

import PrebookSettingsDaysStore from './components/views/PrebookSettingsDays/PrebookSettingsDaysStore'

import GraduationSettingsStore from './components/views/GraduationSettings/GraduationSettingsStore'

import Navigation from './components/views/Navigation/Navigation'
import NavigationStore from './components/views/Navigation/NavigationStore'

import PeriodNamesStore from './components/views/PeriodNames/PeriodNamesStore'

import PeriodsPrefixStore from './components/views/PeriodsPrefix/PeriodsPrefixStore'


import Main from './components/views/Main/Main'

useStrict(true)

WebFontLoader.load({
    google: {
      families: ['Roboto:300,400,500,700', 'Material Icons'],
    },
});

//all stores
const stores = {
    AppointmentTypeSettingsStore,
    AttendanceSettingsStore,
    BlockDateSettingsStore,
    GraduationSettingsStore,
    ImportDataStore,
    ImportGradeSettingsStore,
    NavigationStore,
    PeriodNamesStore,
    PeriodsPrefixStore,
    PrebookSettingsDaysStore,
    PrebookSettingsPeriodsStore,
    PreviewDataStore,
    SchoolBrandingStudentSettingsStore,
    SchoolCalendarSettingsStore,
    SetAdminsStore,
    TechnicalContactStore
}

const root = document.getElementById('root')

render(
    <Provider {...stores}>
        <BrowserRouter>
            <div id='grid'>
                <Navigation />
                <Main />
            </div>
        </BrowserRouter>
    </Provider>,
    root
);