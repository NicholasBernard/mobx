export const initialState = {
    emailAddress: 'TechContact@FakeEmail.com',
    firstName: 'Bob',
    lastName: 'Smith',
    schoolId: '1000',
    title: 'Supreme Techie',
    objectToSave: {},
    isDirty: false
}