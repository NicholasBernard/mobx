export const dataFromServerNoErrors = {
    errorMessages: [],
    importCounts: {
        studentCount: 8,
        staffCount: 6,
        rosterCount: 0,
        departmentCount: 6
    },
    importWasSuccessful: true
}