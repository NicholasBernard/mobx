import { observable } from 'mobx'

export const details = observable([
    {
        isSelected: true,
        periodDescription: 'Test Period 1',
        periodId: 1
    },
    {
        isSelected: true,
        periodDescription: 'Test Period 2',
        periodId: 2
    }
])

export const listOfPeriodsByDay = observable([
    {
        schoolPeriods: observable([
            {
                isSelected: true,
                periodDescription: 'Test Period 1',
                periodId: 1
            },
            {
                isSelected: true,
                periodDescription: 'Test Period 2',
                periodId: 2
            }
        ]),
        weekdayId: 1
    },
    {
        schoolPeriods: observable([
            {
                isSelected: true,
                periodDescription: 'Test Period 1',
                periodId: 1
            },
            {
                isSelected: true,
                periodDescription: 'Test Period 2',
                periodId: 2
            }
        ]),
        weekdayId: 2
    },
    {
        schoolPeriods: observable([
            {
                isSelected: true,
                periodDescription: 'Test Period 1',
                periodId: 1
            },
            {
                isSelected: true,
                periodDescription: 'Test Period 2',
                periodId: 2
            }
        ]),
        weekdayId: 3
    },
    {
        schoolPeriods: observable([
            {
                isSelected: true,
                periodDescription: 'Test Period 1',
                periodId: 1
            },
            {
                isSelected: true,
                periodDescription: 'Test Period 2',
                periodId: 2
            }
        ]),
        weekdayId: 4
    },
    {
        schoolPeriods: observable([
            {
                isSelected: false,
                periodDescription: 'Test Period 1',
                periodId: 1
            },
            {
                isSelected: false,
                periodDescription: 'Test Period 2',
                periodId: 2
            }
        ]),
        weekdayId: 5
    }
])