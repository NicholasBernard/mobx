import {observable} from 'mobx'

export const staffers = observable([
    {
        firstName: "Mark",
        lastName: "Bernard",
        userId: 1,
        userIsActive: false,
        userIsAdmin: true,
        userTypeId: 2
    },
    {
        firstName: "Vince",
        lastName: "Bernard",
        userId: 2,
        userIsActive: false,
        userIsAdmin: true,
        userTypeId: 2
    },
    {
        firstName: "John",
        lastName: "Gerken",
        userId: 3,
        userIsActive: false,
        userIsAdmin: true,
        userTypeId: 2
    },
])

export const details = {
    firstName: 'Mark',
    lastName: 'Bernard',
    userId: 1,
    userIsAdmin: true,
    userTypeId: 2
}