import { observable } from 'mobx'

export const blockedDateFormDefaultState = {
    endDate: new Date(),
    listOfPeriods: [
        {
            isSelected: false,
            periodDescription: 'Test Period 1',
            periodId: 1,
            schoolId: 1,
            schoolPeriodId: 0
        },
        {
            isSelected: false,
            periodDescription: 'Test Period 2',
            periodId: 2,
            schoolId: 1,
            schoolPeriodId: 0
        },
        {
            isSelected: false,
            periodDescription: 'Test Period 3',
            periodId: 3,
            schoolId: 1,
            schoolPeriodId: 0
        },
        {
            isSelected: false,
            periodDescription: 'Test Period 4',
            periodId: 4,
            schoolId: 1,
            schoolPeriodId: 0
        }
    ],
    listOfWeekdays: [
        {
            description: 'Monday',
            id: 1,
            isSelected: false
        },
        {
            description: 'Tuesday',
            id: 2,
            isSelected: false
        },
        {
            description: 'Wednesday',
            id: 3,
            isSelected: false
        },
        {
            description: 'Thursday',
            id: 4,
            isSelected: false
        },
        {
            description: 'Friday',
            id: 5,
            isSelected: false
        }

    ],
    periodsAllChecked: false,
    periodsToInclude: [],
    startDate: new Date(),
    weekdaysAllChecked: false,
    weekdaysToInclude: []
}

export const listOfBlockedDates = observable([
    {
        blockedDetails: observable([
            {
                courseToDateBlockedDetailId: 1001,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 1',
                periodId: 1,
                scheduleDate: '2018-02-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 1002,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 2',
                periodId: 2,
                scheduleDate: '2018-02-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 1003,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 3',
                periodId: 3,
                scheduleDate: '2018-02-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 2001,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 1',
                periodId: 1,
                scheduleDate: '2018-03-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 2002,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 2',
                periodId: 2,
                scheduleDate: '2018-03-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 2003,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 3',
                periodId: 3,
                scheduleDate: '2018-03-06T05:00:00'
            }
        ]),
        blockedReason: 'Blocked Reason Test 1',
        courseToDateBlockedId: 100
    },
    {
        blockedDetails: observable([
            {
                courseToDateBlockedDetailId: 3001,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 1',
                periodId: 1,
                scheduleDate: '2018-05-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 3002,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 2',
                periodId: 2,
                scheduleDate: '2018-05-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 3003,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 3',
                periodId: 3,
                scheduleDate: '2018-05-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 4001,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 1',
                periodId: 1,
                scheduleDate: '2018-05-07T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 4002,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 2',
                periodId: 2,
                scheduleDate: '2018-05-07T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 4003,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 3',
                periodId: 3,
                scheduleDate: '2018-05-07T05:00:00'
            }
        ]),
        blockedReason: 'Blocked Reason Test 2',
        courseToDateBlockedId: 200
    },
])

export const listOfBlockedDatesDateDeleted = observable([
    {
        blockedDetails: observable([
            {
                courseToDateBlockedDetailId: 3001,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 1',
                periodId: 1,
                scheduleDate: '2018-05-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 3002,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 2',
                periodId: 2,
                scheduleDate: '2018-05-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 3003,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 3',
                periodId: 3,
                scheduleDate: '2018-05-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 4001,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 1',
                periodId: 1,
                scheduleDate: '2018-05-07T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 4002,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 2',
                periodId: 2,
                scheduleDate: '2018-05-07T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 4003,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 3',
                periodId: 3,
                scheduleDate: '2018-05-07T05:00:00'
            }
        ]),
        blockedReason: 'Blocked Reason Test 2',
        courseToDateBlockedId: 200
    },
])

export const listOfBlockedDatesDetailDeleted = observable([
    {
        blockedDetails: observable([
            {
                courseToDateBlockedDetailId: 1002,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 2',
                periodId: 2,
                scheduleDate: '2018-02-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 1003,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 3',
                periodId: 3,
                scheduleDate: '2018-02-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 2001,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 1',
                periodId: 1,
                scheduleDate: '2018-03-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 2002,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 2',
                periodId: 2,
                scheduleDate: '2018-03-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 2003,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 3',
                periodId: 3,
                scheduleDate: '2018-03-06T05:00:00'
            }
        ]),
        blockedReason: 'Blocked Reason Test 1',
        courseToDateBlockedId: 100
    },
    {
        blockedDetails: observable([
            {
                courseToDateBlockedDetailId: 3001,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 1',
                periodId: 1,
                scheduleDate: '2018-05-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 3002,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 2',
                periodId: 2,
                scheduleDate: '2018-05-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 3003,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 3',
                periodId: 3,
                scheduleDate: '2018-05-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 4001,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 1',
                periodId: 1,
                scheduleDate: '2018-05-07T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 4002,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 2',
                periodId: 2,
                scheduleDate: '2018-05-07T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 4003,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 3',
                periodId: 3,
                scheduleDate: '2018-05-07T05:00:00'
            }
        ]),
        blockedReason: 'Blocked Reason Test 2',
        courseToDateBlockedId: 200
    },
])

export const blockedDetails = observable([
    {
        courseToDateBlockedDetailId: 1001,
        courseToDateBlockedId: 50,
        periodDescription: 'Test Period 1',
        periodId: 1,
        scheduleDate: '2018-02-06T05:00:00'
    },
    {
        courseToDateBlockedDetailId: 1002,
        courseToDateBlockedId: 50,
        periodDescription: 'Test Period 2',
        periodId: 2,
        scheduleDate: '2018-02-06T05:00:00'
    },
    {
        courseToDateBlockedDetailId: 1003,
        courseToDateBlockedId: 50,
        periodDescription: 'Test Period 3',
        periodId: 3,
        scheduleDate: '2018-02-06T05:00:00'
    },
    {
        courseToDateBlockedDetailId: 2001,
        courseToDateBlockedId: 50,
        periodDescription: 'Test Period 1',
        periodId: 1,
        scheduleDate: '2018-03-06T05:00:00'
    },
    {
        courseToDateBlockedDetailId: 2002,
        courseToDateBlockedId: 50,
        periodDescription: 'Test Period 2',
        periodId: 2,
        scheduleDate: '2018-03-06T05:00:00'
    },
    {
        courseToDateBlockedDetailId: 2003,
        courseToDateBlockedId: 50,
        periodDescription: 'Test Period 3',
        periodId: 3,
        scheduleDate: '2018-03-06T05:00:00'
    }
])

export const blockedDetail = {
    courseToDateBlockedDetailId: 1001,
    courseToDateBlockedId: 50,
    periodDescription: 'Test Period 1',
    periodId: 1,
    scheduleDate: '2018-02-06T05:00:00'
}

export const blockedDatesListItemDetails = observable([
    {
        blockedDetails: observable([
            {
                courseToDateBlockedDetailId: 1001,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 1',
                periodId: 1,
                scheduleDate: '2018-02-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 1002,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 2',
                periodId: 2,
                scheduleDate: '2018-02-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 1003,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 3',
                periodId: 3,
                scheduleDate: '2018-02-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 2001,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 1',
                periodId: 1,
                scheduleDate: '2018-03-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 2002,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 2',
                periodId: 2,
                scheduleDate: '2018-03-06T05:00:00'
            },
            {
                courseToDateBlockedDetailId: 2003,
                courseToDateBlockedId: 50,
                periodDescription: 'Test Period 3',
                periodId: 3,
                scheduleDate: '2018-03-06T05:00:00'
            }
        ]),
        blockedReason: 'Blocked Reason Test 1',
        courseToDateBlockedId: 100
    }
])