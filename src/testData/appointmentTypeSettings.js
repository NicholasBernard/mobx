import { observable } from 'mobx'

export const listOfAppointmentTypes = observable([
    {
        appointmentTypeId: 100,
        description: 'Big Time Appointment Type!',
        hasBeenScheduled: false,
        isActive: true,
        isDefault: false,
        schoolId: 100
    },
    {
        appointmentTypeId: 200,
        description: 'Medium Time Appointment Type!',
        hasBeenScheduled: false,
        isActive: true,
        isDefault: false,
        schoolId: 100
    },{
        appointmentTypeId: 300,
        description: 'Little Time Appointment Type!',
        hasBeenScheduled: false,
        isActive: true,
        isDefault: false,
        schoolId: 100
    }
])

export const listOfAppointmentTypesAfterDelete = observable([
    {
        appointmentTypeId: 100,
        description: 'Big Time Appointment Type!',
        hasBeenScheduled: false,
        isActive: true,
        isDefault: false,
        schoolId: 100
    },{
        appointmentTypeId: 300,
        description: 'Little Time Appointment Type!',
        hasBeenScheduled: false,
        isActive: true,
        isDefault: false,
        schoolId: 100
    }
])

export const appointmentTypeFromForm = {
    appointmentTypeId: 100,
    description: 'Big Time Appointment Type!',
    hasBeenScheduled: false,
    isActive: true,
    isDefault: false,
    schoolId: 100
}

export const appointmentTypeObservable = observable([
    {
        appointmentTypeId: 100,
        description: 'Big Time Appointment Type!',
        hasBeenScheduled: false,
        isActive: true,
        isDefault: false,
        schoolId: 100
    }
])
