import MobxReactForm from 'mobx-react-form'
import validatorjs from 'validatorjs'
import AppointmentTypeStore from './components/views/AppointmentTypeSettings/AppointmentTypeSettingsStore'

const plugins = { dvr: validatorjs }

const fields = [
    {
        name: 'appointmentType',
        label: 'Appointment Type',
        placeholder: 'Add an appointment type',
        rules: 'required|string|between:5,25'
    },
    {
        name: 'isDefault',
        label: 'Is Default',
        //we include the type value here, because passing type: checkbox to the bind
        //argument method is uselss
        type: 'checkbox'
    },
]

const hooks = {
    onSuccess(form) {
        //get field values
        //console.log('Form values!', form.values())
        const appointmentType = {
            appointmentTypeId: 0,
            description: form.values().appointmentType,
            isDefault: form.values().isDefault
        }
        AppointmentTypeStore.saveNewAppointmentType(appointmentType)
    },
    onError(form) {
        //alert('Form has errors!')
        //get all form errors
        //console.log('All form errors', form.errors())
    }
}

export default new MobxReactForm({ fields }, { plugins, hooks }, { name: 'Appointment Type Form'})